# step-project-Forkio
In this project, all students are divided into groups of two. Each member of the team has a list of tasks to complete. Team members can independently decide who will perform task №1 and who will perform task №2.


Assignment for student №1
- Align the site header with the top menu (including the drop-down menu at low screen resolution).
- Create a "People Are Talking About Fork" section.

Assignment for student #2
- Compile the "Revolutionary Editor block". The buttons should be made to look like the one on the right at the top (from there, with the help of the inspector, you can take all the SVG icons and download the styles used on GitHub).
- Create a section "Here is what you get".
- Compile the "Fork Subscription Pricing section". In the block with prices, the third element will always be "highlighted" and will be larger than the others (that is, not on click/hover, but statically).

# Our team:
- Yaroslav Prystupliuk
- Izghashev Volodymyr

# Our responsibilities:
- Yaroslav Prystupliuk - task №1
- Izghashev Volodymyr - task №2
# Еechnologies we use:
- JavaScript ES6
- HTML5
- CSS (SASS, SCSS)
- GULP
- NPM
