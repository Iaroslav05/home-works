// Плагін для картинок через @img
import replace from "gulp-replace";

// Обробка помилок
import plumber from "gulp-plumber";

// Повідомлення підказки
import notify from "gulp-notify";

// Локальний сервер
import browsersync from "browser-sync";

// Оптимізація для нових картинок
import newer from "gulp-newer";

// Умовне розгалуження
import ifPlugin from "gulp-if";

// Eкспортуємий обєкт
export const plugins = {
	replace: replace,
	plumber: plumber,
	notify: notify,
	browsersync: browsersync,
	newer: newer,
	if: ifPlugin
}