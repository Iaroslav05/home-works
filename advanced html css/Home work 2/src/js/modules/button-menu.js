

const menuBtn = document.querySelector('.burger__wrap');
const spanBtn = document.querySelector('.burger__span')
const btmStyle = document.querySelector('.burger__btn');
const menu = document.querySelector('.menu__list');
const menuLinks = document.querySelectorAll('.menu__link');
const body = document.body;

menuBtn.addEventListener('click', () => {
	body.classList.toggle('stop-scroll')
	spanBtn.classList.toggle('burger__span--active');
	btmStyle.classList.toggle('burger__btn--active');
	menu.classList.toggle('menu__list--visible');
});

menuLinks.forEach(function (item) {
	item.addEventListener('click', function (event) {
		body.classList.remove('stop-scroll')
		spanBtn.classList.remove('burger__span--active');
		btmStyle.classList.remove('burger__btn--active');
		menu.classList.remove('menu__list--visible');
		let target = event.target;
		if (!target.classList.contains('menu__link--visible')) {
			menuLinks.forEach(function (item) {
				item.classList.remove('menu__link--visible');
			})
			item.classList.add('menu__link--visible');
		}
	})
})






