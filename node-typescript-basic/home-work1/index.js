const http = require("http");
const commandLineArgs = require("command-line-args");

const optionDefinitions = [{ name: "port", alias: "p", type: Number }];
const options = commandLineArgs(optionDefinitions);
const PORT = options.port || 3000;
let requestCount = 0;

http
  .createServer((req, res) => {
    requestCount++;
    res.end(
      JSON.stringify({
        message: "Request handled successfully",
        requestCount,
      }),
    );
  })
  .listen(PORT, () => {
    console.log("server start:", PORT);
  });
