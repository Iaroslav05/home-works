import fsPromises from "fs/promises";
import path from "path";
enum CRUDEnum {
  CREATE = "create",
  READ = "read",
  UPDATE = "update",
  DELETE = "delete",
  ALL = "all",
}

interface InewspostSchema {
  id: number;
  title: string;
  text: string;
  createDate: string;
  [key: string]: number | string | Date;
}

interface Icontent {
  id: number;
  table: InewspostSchema[];
  schema: InewspostSchema;
}

interface Idata {
  title?: string;
  text?: string;
}

type CRUDresult = Promise<InewspostSchema | Icontent | number | InewspostSchema[]> 

const newspostSchema: InewspostSchema = {
  id: 1,
  title: "",
  text: "",
  createDate: new Date().toISOString(),
};
// Створюємо паттерн Singleton тому що ми створюємо умовно базу даних один раз
class FileDB {
  private static instance: FileDB;
  private constructor() {}

  static getInstance(): FileDB {
    if (!FileDB.instance) {
      FileDB.instance = new FileDB();
    }
    return FileDB.instance;
  }

  async registerSchema(
    tableName: string,
    schema: InewspostSchema
  ): Promise<void> {
    const fileSchema: InewspostSchema = {
      id: 1,
      title: "",
      text: "",
      createDate: "",
    };

    for (const key in schema) {
      fileSchema[key] = typeof schema[key];
    }

    const pathFile = path.join(__dirname, `${tableName}.json`);
    const content: Icontent = {
      id: 1,
      table: [],
      schema: fileSchema,
    };
    await fsPromises.writeFile(pathFile, JSON.stringify(content), {
      encoding: "utf8",
      flag: "w",
    });
  }

  getTable(tableName: string): TableOps {
    return new TableOps(tableName);
  }
}

class Post implements InewspostSchema {
  [key: string]: string | number;

  id: number;
  title: string;
  text: string;
  createDate: string;

  constructor(id: number, title: string, text: string) {
    this.id = id;
    this.title = title;
    this.text = text;
    this.createDate = new Date().toISOString();
  }
}

class TableOps {
  pathFile: string;
  constructor(tableName: string) {
    this.pathFile = path.join(__dirname, `${tableName}.json`);
  }

  async readFile(): Promise<Icontent> {
    const data: string = await fsPromises.readFile(this.pathFile, {
      encoding: "utf8",
      flag: "r",
    });
    return JSON.parse(data);
  }

  async writeFile(content: Icontent): Promise<void> {
    fsPromises.writeFile(this.pathFile, JSON.stringify(content, null, 2), {
      encoding: "utf8",
      flag: "w",
    });
  }

  async create(dataCreate: Idata): Promise<InewspostSchema> {
    const { title, text } = dataCreate;
    const data = await this.readFile();
    let { id, table, schema } = data;

    const keysMatchSchema = Object.keys(dataCreate).every(
      (key) => key in newspostSchema
    );

    if (!keysMatchSchema) {
      throw new Error("Data does not match the schema");
    }

    const newPost: Post = new Post(id, title!, text!);
    table.push(newPost);
    id++;
    await this.writeFile({ id, table, schema });
    return newPost;
  }

  async update(id: number, dataUpdate: Idata): Promise<InewspostSchema> {
    let { title, text } = dataUpdate;
    let data = await this.readFile();
    let { table } = data;

    const keysMatchSchema = Object.keys(dataUpdate).every(
      (key) => key in newspostSchema
    );

    if (!keysMatchSchema) {
      throw new Error("Data does not match the schema");
    }

    const newsToUpdate = table.find((post) => post.id === id);
    if (newsToUpdate) {
      if (title !== undefined) {
        newsToUpdate.title = title;
      }
      if (text !== undefined) {
        newsToUpdate.text = text;
      }
    } else {
      console.log("News not found");
    }
    await this.writeFile({ ...data, table });
    await this.readFile();

    return newsToUpdate!;
  }

  async getById(id: number): Promise<InewspostSchema> {
    const { table } = await this.readFile();
    const post = table.find((post) => post.id === id);
    if (!post) {
      throw new Error(`News post with id ${id} not found`);
    }
    return post;
  }

  async delete(id: number): Promise<number> {
    const data = await this.readFile();
    const { table } = data;
    const newTable = table.filter((post) => {
      return post.id !== id;
    });
    await this.writeFile({ ...data, table: newTable });
    return id;
  }

  async getAll(): Promise<InewspostSchema[]> {
    const { table } = await this.readFile();
    return table;
  }
}

// створив патерн факторі для спрощення визивання CRUD в класі TableOps але невпевнений що воно спростило)))))

class TableOpsFactory {
  static getCRUD(
    type: string,
    tableName: string,
    id?: number,
    dataCreate?: Idata
  ): CRUDresult {
    const tableOps = new TableOps(tableName);
    if (type === CRUDEnum.CREATE) {
      return tableOps.create(dataCreate!);
    }

    if (type === CRUDEnum.READ) {
      return tableOps.getById(id!);
    }

    if (type === CRUDEnum.UPDATE) {
      return tableOps.update(id!, dataCreate!);
    }

    if (type === CRUDEnum.DELETE) {
      return tableOps.delete(id!);
    }

    if (type === CRUDEnum.ALL) {
      return tableOps.getAll();
    }
    return Promise.reject("Invalid CRUD operation type");
  }
}

const data = {
  title: "У зоопарку Чернігова лисичка народила лисеня",
  text: "В Чернігівському заопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!",
};

const data1 = {
  title: "title",
  text: "text",
};

const fileDB = FileDB.getInstance();

fileDB.registerSchema("newspost", newspostSchema);

const table: TableOps = fileDB.getTable("newspost");
console.log(table);

(async () => {
  await TableOpsFactory.getCRUD(CRUDEnum.CREATE, "newspost", undefined, data);
  await TableOpsFactory.getCRUD(CRUDEnum.CREATE, "newspost", undefined, data1);
//   console.log(await TableOpsFactory.getCRUD(CRUDEnum.READ, "newspost", 2));
  const uodate = await TableOpsFactory.getCRUD
  (CRUDEnum.UPDATE, "newspost", 2, {title: "Маленька лисичка", text: "В Рівному в зоопарку" });
  console.log(uodate);
  
// await TableOpsFactory.getCRUD(CRUDEnum.DELETE, "newspost", 1);
// console.log( await TableOpsFactory.getCRUD(CRUDEnum.ALL, "newspost"));
})();


// (async () => {
//   await table.create(data);
//   await table.create(data1);
//   await table.update(2, {
//     title: "Маленька лисичка",
//     text: "В Рівному в зоопарку",
//   });
//   await table.delete(1);

//   await table.getAll();
//   console.log(await table.getById(2));
// })();
