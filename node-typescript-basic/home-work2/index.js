const fsPromises = require("fs/promises");
const path = require("path");

const newspostSchema = {
  id: Number,
  title: String,
  text: String,
  createDate: Date,
};

class FileDB {
  async registerSchema(tableName, schema) {
    const fileSchema = {};
    for (const key in schema) {
      fileSchema[key] = typeof schema[key]();
    }
    const pathFile = path.join(__dirname, `${tableName}.json`);
    const content = {
      id: 1,
      table: [],
      schema: fileSchema,
    };
    try {
      await fsPromises.writeFile(pathFile, JSON.stringify(content), {
        encoding: "utf8",
        flag: "w",
      });
    } catch (err) {
      console.error(err);
    }
  }

  getTable(tableName) {
    return new TableOps(tableName);
  }
}

class Post {
  id;
  title;
  text;
  createDate = new Date().toISOString();

  constructor(id, title, text) {
    this.id = id;
    this.title = title;
    this.text = text;
  }
}

class TableOps {
  pathFile;
  constructor(tableName) {
    this.pathFile = path.join(__dirname, `${tableName}.json`);
  }

  async readFile() {
    try {
      const data = await fsPromises.readFile(this.pathFile, {
        encoding: "utf8",
      });
		console.log("return readFile", JSON.parse(data));
      return JSON.parse(data);
    } catch (err) {
      console.error(err);
    }
  }

  async writeFile(content) {
    try {
      fsPromises.writeFile(this.pathFile, JSON.stringify(content, null, 2), {
        encoding: "utf8",
        flag: "w",
      });
    } catch (err) {
      console.error(err);
    }
  }

  async create(data) {
    const { title, text } = data;
    let { id, table, schema } = await this.readFile();
    const newPost = new Post(id, title, text);
    table.push(newPost);
    const content = {
      id: 1,
      table: table.map((item, index) => ({ ...item, id: index + 1 })),
      schema,
    };
    await this.writeFile(content);
    return newPost;
  }

  async update(id, data) {
    let { title, text } = data;
    let { schema, table } = await this.readFile();
    const newsToUpdate = table.find((post) => post.id === id);
    if (newsToUpdate) {
      newsToUpdate.title = title;
      newsToUpdate.text = text;
    } else {
      console.log("News not found");
    }
   await this.writeFile({ table, schema });
	const newNews = await this.readFile();
	  
	 return newNews;
  }

async delete(id){
let { table, schema } = await this.readFile();
table = table.filter((post) => post.id !== id);
await this.writeFile({ table, schema });
}

  async getAll() {
	const { table } = await this.readFile();
	return table
  }
}

const data = {
  title: "У зоопарку Чернігова лисичка народила лисеня",
  text: "В Чернігівському заопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!",
  hi: "hi",
};

const data1 = {
  title: "title",
  text: "text",
  go: 444,
};

const fileDB = new FileDB();
fileDB.registerSchema("newspost", newspostSchema);
const table = fileDB.getTable("newspost");

(async () => {
  try {
    await table.create(data);
    await table.create(data1);
	 await table.update(2, {title: "Маленька лисичка", text: "В Рівному"});
	 await table.delete(3);
	//  await table.getAll();
  } catch (error) {
    console.error(error);
  }
})();

