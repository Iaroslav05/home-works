import fsPromises from "fs/promises";
import path from "path";

interface InewspostSchema {
  id: number;
  title: string;
  text: string;
  createDate: Date;
  [key: string]: number | string | Date;
}

interface Icontent {
  id: number;
  table: InewspostSchema[];
  schema: InewspostSchema;
}

interface Idata {
	title: string;
	text: string
}

const newspostSchema: InewspostSchema = {
  id: 0,
  title: "",
  text: "",
  createDate: new Date(),
};


class FileDB {
  async registerSchema(tableName: string, schema: InewspostSchema): Promise<void> {
    const fileSchema: InewspostSchema = {
      id: 1,
      title: "",
      text: "",
      createDate: new Date(),
    };
 for (const key in schema) {
   fileSchema[key] = typeof schema[key];
 }
    const pathFile:string = path.join(__dirname, `${tableName}.json`);
    const content: Icontent = {
      id: 1,
      table: [],
      schema: fileSchema,
    };
    try {
      await fsPromises.writeFile(pathFile, JSON.stringify(content), {
        encoding: "utf8",
        flag: "w",
      });
    } catch (err: unknown | string) {
      console.error(err);
    }
  }

  getTable(tableName: string) {
    return new TableOps(tableName);
  }
}

class Post {
  id: number;
  title: string;
  text: string;
  createDate = new Date().toISOString();

  constructor(id: number, title: string, text: string) {
    this.id = id;
    this.title = title;
    this.text = text;
  }
}

class TableOps {
  pathFile: string;
  constructor(tableName: string) {
    this.pathFile = path.join(__dirname, `${tableName}.json`);
  }

  async readFile() {
    try {
      const data: string = await fsPromises.readFile(this.pathFile, {
        encoding: "utf8",
      });
      return JSON.parse(data);
    } catch (err) {
      console.error(err);
    }
  }

  async writeFile(content: Icontent): Promise<void> {
    try {
      fsPromises.writeFile(this.pathFile, JSON.stringify(content, null, 2), {
        encoding: "utf8",
        flag: "w",
      });
    } catch (err) {
      console.error(err);
    }
  }

  async create(data: Idata) {
    const { title, text } = data;
    let { id, table, schema } = await this.readFile();
    const newPost = new Post(id, title, text);
    table.push(newPost);
    const content: Icontent = {
      id: 1,
      table: table.map((item: number[], index: number) => ({ ...item, id: index + 1 })),
      schema,
    };
    await this.writeFile(content);
    return newPost;
  }

  async update(id: number, data: Idata): Promise<Idata[]> {
    let {title, text } = data;
    let { schema, table } = await this.readFile();
    const newsToUpdate: Post = table.find((post: Post) => post.id === id);
    if (newsToUpdate) {
      newsToUpdate.title = title;
      newsToUpdate.text = text;
    } else {
      console.log("News not found");
    }
    await this.writeFile({ id: 1, table, schema });
    const newNews: Idata[] = await this.readFile();

    return newNews;
  }

  async delete(id: number) {
    let { schema, table } = await this.readFile();
    table = table.filter((post: Post) => post.id !== id);
    await this.writeFile({ id, table, schema });
  }

  async getAll() {
    const { table } = await this.readFile();
    return table;
  }
}

const data = {
  title: "У зоопарку Чернігова лисичка народила лисеня",
  text: "В Чернігівському заопарку сталася чудова подія! Лисичка на ім'я Руда народила чудове лисенятко! Тож поспішайте навідатись та подивитись на це миле створіння!",
  hi: "hi",
};

const data1 = {
  title: "title",
  text: "text",
  go: 444,
};

const fileDB = new FileDB();
fileDB.registerSchema("newspost", newspostSchema);
const table = fileDB.getTable("newspost");

(async () => {
  try {
    await table.create(data);
    await table.create(data1);
    await table.update(2, { title: "Маленька лисичка", text: "В Рівному" });
    await table.delete(1);
	   await table.create(data);
   //  await table.getAll();
  } catch (error) {
    console.error(error);
  }
})();
