const databaseSchema = [
  {
    name: "newsposts",
    fields: [
      { name: "id", type: "increments" },
      { name: "title", type: "string" },
      { name: "text", type: "string" },
      { name: "createDate", type: "timestamptz" },
    ],
  },
];

export default databaseSchema