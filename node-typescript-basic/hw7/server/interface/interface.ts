export interface IParam {
  page: number;
  size: number;
}