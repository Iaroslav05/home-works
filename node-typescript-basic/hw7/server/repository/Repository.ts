import { knex, Knex } from "knex";
import path from "path";
import databaseSchema from "../schema";
import { IParam } from "../interface/interface";

class Repository {
  private static instance: Repository;
  private db: Knex;

  constructor() {
    this.db = knex({
      client: "sqlite3",
      connection: {
        filename: path.join(__dirname, "../DB/fileDB.sqlite"),
      },
      useNullAsDefault: true,
    });
  }

  public static getInstance(): Repository {
    if (!Repository.instance) {
      Repository.instance = new Repository();
    }
    return Repository.instance;
  }

  async createTables() {
    for (const schema of databaseSchema) {
      const tableExists = await this.db.schema.hasTable(schema.name);

      if (!tableExists) {
        await this.db.schema.createTable(
          schema.name,
          (table: Knex.CreateTableBuilder) => {
            for (const field of schema.fields) {
              if (field.type === "increments") {
                table.increments(field.name);
              } else if (field.type === "string") {
                table.string(field.name);
              } else if (field.type === "timestamptz") {
                table.timestamp(field.name).defaultTo(new Date().toISOString());
              }
            }
          }
        );
      }
    }
  }

  async devDropTables() {
    for (const schema of databaseSchema) {
      const tableExists = await this.db.schema.hasTable(schema.name);

      if (tableExists) {
        await this.db.schema.dropTable(schema.name);
      }
    }
  }

  async count(table: string) {
    return this.db
      .select()
      .from(table)
      .count()
      .then((rows: any) => rows[0]["count(*)"]);
  }

  async create(table: string, data: any) {
    return this.db
      .insert(data)
      .into(table)
      .returning("*")
      .then((rows: any) => rows[0]);
  }

  async readAll(
    table: string,
    params: IParam ) {
		const items = await this.db
      .select()
      .from(table)
      .limit(params.size)
      .offset(params.page)
      .returning("*");

    return items;
  }

  async read(table: string, id: number) {
    return this.db
      .select()
      .from(table)
      .where("id", id)
      .returning("*")
      .then((rows: any) => rows[0]);
  }

  async update(table: string, id: number, newData: any) {
    return this.db(table).where("id", id).update(newData);
  }

  async delete(table: string, id: number) {
    return this.db(table).where("id", id).del();
  }
}

export default Repository.getInstance();
