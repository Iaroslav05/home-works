import axios from "axios";

const main = async () => {
  const URL = "http://localhost:8000/api";


  const createNews = await axios.post(`${URL}/newsposts`, {
    title: "Hello",
    text: "World",
  });

  await axios.post(`${URL}/newsposts`, {
    title: "Hello",
    text: "Hello people this is first " + Math.floor(Math.random() * 1000),
  });
  await axios.post(`${URL}/newsposts`, {
    title: "Hello",
    text: "Hello people this is second " + Math.floor(Math.random() * 1000),
  });
  await axios.post(`${URL}/newsposts`, {
    title: "Hello",
    text: "Hello people this is three " + Math.floor(Math.random() * 1000),
  });
  console.log("createPost", createNews.data);

  const getPost = await axios.get(
    `${URL}/newsposts/${createNews.data.data.id}`
  );
  console.log("getPost", getPost.data);

  const updateNews = await axios.put(
    `${URL}/newsposts/${createNews.data.data.id}`,
    {
      title: "Hello",
      text: "World2",
    }
  );

  console.log("updateNews", updateNews.data);

  const deleteNews = await axios.delete(
    `${URL}/newsposts/${createNews.data.data.id}`
  );
  console.log("deletePost", deleteNews.data);

  // get all posts

  const getNews = await axios.get(`${URL}/newsposts`);

  console.log("getNews", getNews.data);

  console.table(getNews.data.data);
};

main();
