import { useEffect, useState } from "react";
import domain from "../config/API";
import { IItem } from "../interface/item";
import { useParams } from "react-router-dom";

export const useEditNewspost = ({title, text,}: {title: string; text: string;}) => {

	const {id} = useParams();
 if (!id) {
   throw new Error("No id provided");
 }

  const [editNewsPosts, setEditNewsPosts] = useState<IItem>({} as IItem);
useEffect(() => {
  const editNewspost = async () => {
    try {
      const response = await fetch(`${domain}/newsposts/${id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          title,
          text,
        }),
      });
      if (!response.ok) {
        throw new Error(`Error Api: status ${response.status}`);
      }
      const { data }: { data: IItem } = await response.json();
      setEditNewsPosts(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  editNewspost();
},[id, text, title]);

  return editNewsPosts;
};
