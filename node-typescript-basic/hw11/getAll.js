const { Newsposts } = require("./entities/newspost");
const { sequelize } = require("./sequelize");

const getAll = async () => {
	try {
		const posts = await Newsposts.findAll();
		console.log(posts);
	} catch (error) {
		console.error(error);
	} finally {
		await sequelize.close();
	}
}
getAll();