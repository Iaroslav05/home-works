const { Newsposts } = require("./entities/newspost");
const { sequelize } = require("./sequelize");
const commandLineArgs = require("command-line-args");

const optionDefinitions = [
  { name: "title", alias: "t", type: String },
  { name: "text", alias: "p", type: String },
];
const options = commandLineArgs(optionDefinitions);

const createPost = async () => {
  const postData = {
    title: options.title,
    text: options.text,

    created_date: new Date(),
  };

  try {
    const createdPost = await Newsposts.create(postData);
    console.log(createdPost.toJSON());
  } catch (error) {
    console.error(error);
  } finally {
    await sequelize.close();
  }
};
createPost();
