const { Newsposts } = require("./entities/newspost");
const { sequelize } = require("./sequelize");
const commandLineArgs = require("command-line-args");

const optionDefinitions = [
  { name: "id", alias: "i", type: Number },
];
const options = commandLineArgs(optionDefinitions);

const getById = async (id) => {
	try {
		const post = await Newsposts.findByPk(id);
		console.log(post);
	} catch (error) {
		console.error(error);
	} finally {
		await sequelize.close();
	}
}
getById(options.id);