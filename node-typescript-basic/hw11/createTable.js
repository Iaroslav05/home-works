const { NewsPosts } = require("./entities/newspost");
const { sequelize } = require("./sequelize");

const createdTable = async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
NewsPosts
    const tableNames = await sequelize.getQueryInterface().showAllTables();
    if (tableNames.includes("NewsPosts")) {
      console.log("Table created");
    } else {
      console.log("Table not created");
    }

    await sequelize.sync({ force: true });
	 
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  } finally {
    await sequelize.close();
  }
};

createdTable();
