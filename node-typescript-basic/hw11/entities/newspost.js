const Sequelize = require("sequelize");
const {sequelize} = require("../sequelize.js");

const Newsposts = sequelize.define(
  "NewsPosts",
  {
    id: {
      type: Sequelize.DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: Sequelize.DataTypes.STRING,
    },
    text: {
      type: Sequelize.DataTypes.STRING,
    },
    created_date: {
      type: Sequelize.DataTypes.DATE,
    },
  });

  module.exports = {
	Newsposts
}
