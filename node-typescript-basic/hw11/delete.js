const { Newsposts } = require("./entities/newspost");
const { sequelize } = require("./sequelize");
const commandLineArgs = require("command-line-args");

const optionDefinitions = [
  { name: "id", alias: "i", type: Number },
];
const options = commandLineArgs(optionDefinitions);

const deleteById = async (id) => {
	try {
		const deletedPost = await Newsposts.destroy({ where: { id } });
		console.log(options.id, "deleted", deletedPost);
	} catch (error) {
		console.error(error);
	} finally {
		await sequelize.close();
	}
}

deleteById(options.id);