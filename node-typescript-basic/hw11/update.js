const { Newsposts } = require("./entities/newspost");
const { sequelize } = require("./sequelize");
const commandLineArgs = require("command-line-args");



const optionDefinitions = [
  { name: "id", alias: "i", type: Number, multiple: true },
  { name: "title", alias: "t", type: String, multiple: true },
  { name: "text", alias: "p", type: String, multiple: true },
];
const options = commandLineArgs(optionDefinitions);

const updatePost = async () => {
  try {
    for (let i = 0; i < options.id.length; i++) {
      const updateFields = {};
      if (options.title && options.title[i]) {
        updateFields.title = options.title[i];
      }
      if (options.text && options.text[i]) {
        updateFields.text = options.text[i];
      }

      const updatedPost = await Newsposts.update(updateFields, {
        where: { id: options.id[i] },
      });
      console.log(options.id[i], "updated", updatedPost);
    }
  } catch (error) {
    console.error(error);
  } finally {
    await sequelize.close();
  }
};

updatePost();
