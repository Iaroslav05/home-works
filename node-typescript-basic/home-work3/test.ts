import { CurrencyEnum, Card } from "./index";


describe("Card class", () => {
  let card;

  beforeEach(() => {
    card = new Card();
  });

  test("should add a transaction to the list", () => {
    const transactionId = card.addTransaction(CurrencyEnum.USD, 100);
    expect(card.listTransaction.length).toBe(1);
    expect(card.listTransaction[0].amount).toBe(100);
    expect(card.listTransaction[0].currency).toBe(CurrencyEnum.USD);
    expect(card.listTransaction[0].id).toBe(transactionId);
  });

  test("should get a transaction by id", () => {
    const transactionId = card.addTransaction(CurrencyEnum.USD, 100);
    const transaction = card.getTransaction(transactionId);
    expect(transaction).toBeDefined();
    expect(transaction.id).toBe(transactionId);
  });

  test("should calculate balance in USD", () => {
    card.addTransaction(CurrencyEnum.USD, 100);
    card.addTransaction(CurrencyEnum.USD, 200);
    expect(card.getBalance(CurrencyEnum.USD)).toBe(300);
  });

  test("should calculate balance in UAH", () => {
    card.addTransaction(CurrencyEnum.UAH, 1000);
    card.addTransaction(CurrencyEnum.UAH, 2000);
    expect(card.getBalance(CurrencyEnum.UAH)).toBe(3000);
  });
});