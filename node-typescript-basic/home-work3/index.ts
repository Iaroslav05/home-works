import { v4 as uuidv4 } from "uuid";

enum CurrencyEnum {
  USD = "USD",
  UAH = "UAH"
}

class Transaction {
  id: string = uuidv4();
  amount: number;
  currency: CurrencyEnum;

  constructor(amount: number, currency: CurrencyEnum) {
    this.amount = amount;
    this.currency = currency;
  }
}

class Card {
  listTransaction: Transaction[] = [];

  addTransaction(transaction: Transaction): string;
  addTransaction(currency: CurrencyEnum, amount: number): string;

  addTransaction(transactionOrCurrency: Transaction | CurrencyEnum, amount?: number): string {
    if (transactionOrCurrency instanceof Transaction) {
      this.listTransaction.push(transactionOrCurrency);
      return transactionOrCurrency.id;
    } else if (transactionOrCurrency && amount !== undefined) {
      const transaction = new Transaction(amount, transactionOrCurrency);
      this.listTransaction.push(transaction);
      return transaction.id;
    } else {
      throw new Error("Invalid arguments");
    }
  }

  getTransaction(id: string) {
    if (id) {
       return this.listTransaction.find((transaction) => transaction.id === id);
    }
  }

  getBalance(currency: CurrencyEnum) {
    return this.listTransaction.reduce((accum, transaction) => {
      if (transaction.currency === currency) {
        return accum + transaction.amount;
      } else {
        return accum;
      }
    }, 0);
  }
}

const card = new Card();
const transactionUsd = card.addTransaction(new Transaction(100, CurrencyEnum.USD));
card.addTransaction(CurrencyEnum.USD, 50);
card.addTransaction(CurrencyEnum.USD, 150);
console.log(transactionUsd);
console.log(card.getBalance(CurrencyEnum.USD));

const transactionUah = card.addTransaction(new Transaction(300, CurrencyEnum.UAH));
card.addTransaction(CurrencyEnum.UAH, 50);
console.log(transactionUah);
console.log(card.getBalance(CurrencyEnum.UAH));

export { CurrencyEnum, Transaction, Card};