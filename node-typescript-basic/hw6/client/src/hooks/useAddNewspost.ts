import { useState } from "react";
import domain from "../config/API";
import { IItem } from "../interface/item";

export const useAddNewspost = ({title, text}: {title: string, text: string}) => {

  const [addNewsPosts, setAddNewsPosts] = useState<IItem>({} as IItem);

    const addNewspost = async () => {
      try {
        const response = await fetch(`${domain}/newsposts`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            title,
            text,
          }),
        });
        if (!response.ok) {
          throw new Error(`Error Api: status ${response.status}`);
        }
        const { data }: { data: IItem } = await response.json();
        setAddNewsPosts(data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
return { addNewsPosts, addNewspost };

};