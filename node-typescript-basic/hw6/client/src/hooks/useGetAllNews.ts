import { useEffect, useState } from "react"
import domain from "../config/API";
import { IItem } from "../interface/item";

export const useGetAllNews = (): IItem[] => {
  const [newsPosts, setNewsPosts] = useState<IItem[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`${domain}/newsposts`);
        if (!response.ok) {
          throw new Error(`Error Api: status ${response.status}`);
        }
        const { data }: { data: { items: IItem[] } } = await response.json();
        setNewsPosts(data.items);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  return newsPosts;
};