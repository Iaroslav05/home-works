import { FC } from "react"
import Button from "../components/Button";
import CardsList from "../components/CardsList";
import { Link } from "react-router-dom";

const HomePage: FC = () => {
  return (
    <>
      <Link style={{ textDecoration: "none" }} to={"/new"}>
        <Button buttonText="Add news" />
      </Link>
      <div style={{ display: "flex", gap: "10px", flexWrap: "wrap" }}>
        <CardsList />
      </div>
    </>
  );
}

export default HomePage
