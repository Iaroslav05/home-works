export interface IItem {
  id: string;
  title: string;
  text: string;
  createDate: string;
}

export interface BtnProps {
  buttonText: string;
  color?: "primary" | "secondary" | "success" | "error" | "info" | "warning";
  variant?: "text" | "outlined" | "contained";
  onClick?: (() => void);
}

export interface IState {
  newsposts: IItem[];
  loading: boolean;
  error: string | null;
}

export interface OneNewspostsState {
  oneNewsposts: IItem | null;
  loading: boolean;
  error: string | null;
}