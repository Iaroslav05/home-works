import { FC } from "react";
import { Link } from "react-router-dom";
import CardNews from "../Card/CardNews";
import { IItem } from "../../interface/item";
import { useGetAllNews } from "../../hooks/useGetAllNews";

const CardsList: FC = () => {
const newsposts = useGetAllNews();

  return (
    <>
      {newsposts.map((item: IItem, index: number) => (
        <Link style={{ textDecoration: "none" }} key={index} to={`/${item.id}`}>
          <CardNews news={item} />
        </Link>
      ))}
    </>
  );
};

export default CardsList;
