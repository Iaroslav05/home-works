import express, { Application } from "express";
import path from "path";
import dotenv from "dotenv";
import morgan from "morgan";
import bodyParser from "body-parser";
import { MainController } from "./controllers";
import Routes from "./routers";
import {DatabaseService} from "./services";
import cors from "cors";

dotenv.config();

const PORT: number | string = process.env.PORT || 3000;
const HOST: string = process.env.HOST || "localhost";
const CORS_OPTIONS = {
  origin: "http://localhost:3000",
  optionsSuccessStatus: 200,
};


class App {
  private app: Application;
  private publicPath: string;
  private mainController: MainController;

  constructor() {
    this.app = express();
    this.mainController = new MainController();
    this.publicPath = path.join(__dirname, "../client/build");
  }

  private routing(): void {
    this.app.use(express.static(path.join(this.publicPath)));
    this.app.get("/", this.mainController.getStartPage);

    Object.keys(Routes).forEach((key: string) => {
      this.app.use(`/api/${key}`, Routes[key]);
    });
  }

  initPlugins(): void {
    this.app.use(bodyParser.json());
    this.app.use(morgan("dev"));
	 this.app.use(cors(CORS_OPTIONS));
  }

  async start(): Promise<void> {
    if (process.env.NODE_ENV !== "production") {
      await DatabaseService.devDropTables(); // for dev
      await DatabaseService.createTables();
    }

    this.initPlugins();
    this.routing();

    this.app.listen(PORT, () => {
      console.log(`Server start: ${HOST}:${PORT}`);
    });
  }
}
const server = new App();
server.start();
