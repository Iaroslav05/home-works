import { knex, Knex } from "knex";
import path from "path";

const DatabaseSchema = [
  {
    name: "newsposts",
    fields: [
      { name: "id", type: "increments" },
      { name: "title", type: "string" },
      { name: "text", type: "string" },
      { name: "createDate", type: "timestamptz" },
    ],
  },
];

class DatabaseService {
  private static instance: DatabaseService;
  private db: Knex;

  constructor() {
    this.db = knex({
      client: "sqlite3",
      connection: {
        filename: path.join(__dirname, "../DB/fileDB.sqlite"),
      },
      useNullAsDefault: true,
    });
  }

  public static getInstance(): DatabaseService {
    if (!DatabaseService.instance) {
      DatabaseService.instance = new DatabaseService();
    }
    return DatabaseService.instance;
  }

  async createTables() {
    for (const schema of DatabaseSchema) {
      const tableExists = await this.db.schema.hasTable(schema.name);

      if (!tableExists) {
        // створення таблиці
        await this.db.schema.createTable(schema.name, (table: any) => {
          for (const field of schema.fields) {
            if (field.type === "increments") {
              table.increments(field.name);
            } else if (field.type === "string") {
              table.string(field.name);
            } else if (field.type === "timestamptz") {
              table.timestamp("createDate").defaultTo(new Date().toISOString());
            }
          }
        });
      }
    }
  }

  async devDropTables() {
    for (const schema of DatabaseSchema) {
      const tableExists = await this.db.schema.hasTable(schema.name);

      if (tableExists) {
        await this.db.schema.dropTable(schema.name);
      }
    }
  }

  async count(table: string) {
    return this.db
      .select()
      .from(table)
      .count()
      .then((rows: any) => rows[0]["count(*)"]);
  }

  async create(table: string, data: any) {
    return this.db
      .insert(data)
      .into(table)
      .returning("*")
      .then((rows: any) => rows[0]);
  }

  async readAll(
    table: string,
    params: {
      skip: number;
      limit: number;
    }
  ) {
    const items = await this.db
      .select()
      .from(table)
      .limit(params.limit)
      .offset(params.skip)
      .returning("*");

    return items;
  }

  async read(table: string, id: number) {
    return this.db
      .select()
      .from(table)
      .where("id", id)
      .returning("*")
      .then((rows: any) => rows[0]);
  }

  async update(table: string, id: number, newData: any) {
    console.log(table, id, newData);

    return this.db(table).where("id", id).update(newData);
  }

  async delete(table: string, id: number) {
    return this.db(table).where("id", id).del();
  }
}


export default DatabaseService.getInstance();