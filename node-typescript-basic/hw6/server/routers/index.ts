import { Router } from "express";
import NewsRoute from "./news.routes";

interface IRouter {
  [key: string]: Router;
}

const Routes: IRouter = {
  newsposts: NewsRoute,
};

export default Routes;
