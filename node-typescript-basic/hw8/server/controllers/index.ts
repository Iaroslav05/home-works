import { MainController } from "./main.controller";
import { NewsController } from "./news.controlles";
import { BaseController } from "./base.controller";

export { MainController, NewsController, BaseController };
