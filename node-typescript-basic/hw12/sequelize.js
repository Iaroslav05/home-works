const {Sequelize} = require('sequelize');
const dotenv = require('dotenv');

dotenv.config();

const username = process.env.POSTGRES_USER;
const password = process.env.POSTGRES_PASSWORD;
const database = process.env.POSTGRES_DB;

		const sequelize = new Sequelize(database, username, password, {
      dialect: "postgres",
      host: "localhost",
      port: 5432,
      logging: false,
    });
;

module.exports = {
	sequelize
}
