const Sequelize = require("sequelize");
const { sequelize } = require("../sequelize.js");

const Videos = sequelize.define("videos", {
  id: {
    type: Sequelize.DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: Sequelize.DataTypes.STRING,
  },
  views: {
    type: Sequelize.DataTypes.REAL,
  },
  category: {
    type: Sequelize.DataTypes.STRING,
  },
});

module.exports = {
  Videos,
};
