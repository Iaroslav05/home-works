const { Videos } = require("./entities/videos");
const { sequelize } = require("./sequelize");

const createdTable = async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
Videos
    const tableNames = await sequelize.getQueryInterface().showAllTables();
    if (tableNames.includes("videos")) {
      console.log("Table not created");
    } else {
      console.log("Table created");
    }

    await sequelize.sync({ force: true });
	 
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  } finally {
    await sequelize.close();
  }
};

createdTable();
