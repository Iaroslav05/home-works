const { Videos } = require("./entities/videos");
const { sequelize } = require("./sequelize");
const commandLineArgs = require("command-line-args");

const optionDefinitions = [{ name: "top", alias: "t", type: Number }];
const options = commandLineArgs(optionDefinitions);

const getTopVideos = async () => {
  try {
    const topViews = await Videos.findAll({
      order: [["views", "DESC"]],
      limit: options.top,
    });
	console.log(topViews.map((view) => view.toJSON()));
  } catch (error) {
    console.error(error);
  } finally {
    await sequelize.close();
  }
};

getTopVideos();
