const { Videos } = require("./entities/videos");
const { sequelize } = require("./sequelize");


const getViewsCategory = async () => {
  try {
    const viewsCategory = await Videos.findAll({
		group: ["category"],
		attributes: [
		  "category",
		  [sequelize.fn("sum", sequelize.col("views")), "total_views"],
		],
	 })
	 console.log(viewsCategory.map((view) => view.toJSON()));
  } catch (error) {
    console.error(error);
  } finally {
    await sequelize.close();
  }
};

getViewsCategory();
