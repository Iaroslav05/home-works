export default {
  name: "users",
  fields: [
    { name: "id", type: "increments" },
    { name: "email", type: "string" },
    { name: "password", type: "string" },
    { name: "confirmPassword", type: "string" },
  ],
};
