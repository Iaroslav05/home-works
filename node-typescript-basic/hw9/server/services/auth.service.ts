import {AuthRepository} from "../repository";

class AuthService {
  private static instance: AuthService;

  public static getInstance(): AuthService {
    if (!AuthService.instance) {
      AuthService.instance = new AuthService();
    }
    return AuthService.instance;
  }
  async login(token: string, done: (err: Error | null, user?: any) => void) {
    return await AuthRepository.authCallback(token, done);
  }
  hash(str: string) {
    return AuthRepository.hash(str);
  }
}

export default AuthService.getInstance()