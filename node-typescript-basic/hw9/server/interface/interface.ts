export interface IParam {
  page: number;
  size: number;
}

export interface IUsers {
  id: number;
  email: string;
  password: string;
  confirmPassword: string;
}