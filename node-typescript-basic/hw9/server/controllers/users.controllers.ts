import { Request, Response } from "express";
import { AuthService, DatabaseService } from "../services";
import { BaseController } from "./base.controller";


export class UsersController extends BaseController {
  constructor() {
	super("users");
	}

  async registration(req: Request, res: Response) {

    try {
      const {password, confirmPassword } = req.body;
	
      if (password !== confirmPassword) {
        res.status(400);
        return res.json({ message: `Passwords do not match` });
      }

      req.body.email = req.body.email.toLowerCase();
      req.body.password = AuthService.hash(password);

      const data = await DatabaseService.create("users", req.body);

      res.status(201);
      res.json({ message: `users created`, data });
    } catch (error) {
      res.status(500);
      res.json({ message: `Error creating users` });
    }
  }
}
