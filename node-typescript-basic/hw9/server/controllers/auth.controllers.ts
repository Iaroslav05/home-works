import { Request, Response } from "express";


class AuthController {

  async generateToken(req: Request, res: Response) {
    try {

      const { email, password } = req.body;
      const salt = process.env.SALT || "12345";
      const unHashedToken = `${email}:${password}:${salt}`;
      const hash = Buffer.from(unHashedToken).toString("base64");

      res.send({ token: hash });
    } catch (error) {
      console.error("Error generating token:", error);
      res.status(500).json({ message: "Error generating token" });
    }
  }
  
}

export {AuthController}