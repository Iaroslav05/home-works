import { knex, Knex } from "knex";
import path from "path";

class DatabaseConnection {
  private static instance: DatabaseConnection;
  private db: Knex;

  private constructor() {
    this.db = knex({
      client: "sqlite3",
      connection: {
        filename: path.join(__dirname, "../DB/fileDB.sqlite"),
      },
      useNullAsDefault: true,
    });
  }

  public static getInstance(): DatabaseConnection {
    if (!DatabaseConnection.instance) {
      DatabaseConnection.instance = new DatabaseConnection();
    }
    return DatabaseConnection.instance;
  }

  public getDB(): Knex {
    return this.db;
  }
}

export default DatabaseConnection.getInstance();
