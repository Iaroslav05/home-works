import { Knex } from "knex";
import DatabaseConnection from "./DatabaseConnection";
import crypto from "crypto";
import { IUsers } from "../interface/interface";

class AuthRepository {
  private static instance: AuthRepository;
  private db: Knex;

  constructor() {
    this.db = DatabaseConnection.getDB();
  }

  hash(password: string) {
    return crypto.createHash("sha256").update(password).digest("hex");
  }

  authCallback = async (
    token: string,
    done: (err: Error | null, user?: IUsers | boolean) => void
  ) => {
    const unHashedToken = Buffer.from(token, "base64").toString("utf-8");

    let [email, password] = unHashedToken.split(":");

    password = this.hash(password);

    let err: Error | null = null;

    const user = await this.db
      .select()
      .from("users")
      .where("email", email)
      .returning("*")
      .then((rows: any) => rows[0]);

    if (err) {
      return done(err);
    }

    if (!user) {
      return done(null, false);
    }

    if (user.password !== password) {
      return done(null, false);
    }
    return done(null, user);
  };

  public static getInstance() {
    if (!AuthRepository.instance) {
      AuthRepository.instance = new AuthRepository();
    }
    return AuthRepository.instance;
  }
}
export default AuthRepository.getInstance()