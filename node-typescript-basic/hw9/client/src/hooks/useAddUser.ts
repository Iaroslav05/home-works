import { useState } from "react";
import domain from "../config/API";
import { IUser } from "../interface/item";

export const useAddNewUser = ({
  email,
  password,
  confirmPassword,
}: {
  email: string;
  password: string;
  confirmPassword: string;
}) => {
  const [addNewUser, setAddNewUser] = useState<IUser>({} as IUser);

  const addNewUsers = async () => {
    try {
      const response = await fetch(`${domain}/auth/register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email,
          password,
          confirmPassword,
        }),
      });
      if (!response.ok) {
        throw new Error(`Error Api: status ${response.status}`);
      }
      const { data }: { data: IUser } = await response.json();
      console.log(data);

      setAddNewUser(data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  return { addNewUser, addNewUsers };
};