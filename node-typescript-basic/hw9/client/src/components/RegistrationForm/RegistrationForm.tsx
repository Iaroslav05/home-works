import { FC, useState } from "react";
import { Box, TextField, Typography } from "@mui/material";
import { useGoBack } from "../../hooks/useGoBack";

import Button from "../Button";
import { Link } from "react-router-dom";
import { useAddNewUser } from "../../hooks/useAddUser";

const RegistrationForm: FC = () => {
  const goBack = useGoBack();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const { addNewUsers } = useAddNewUser({ email, password, confirmPassword });

  const handleCreateUser = () => {
    addNewUsers();

    setEmail("");
    setPassword("");
    setConfirmPassword("");
  };

  return (
    <>
      <div>
        <Typography variant="h4" align="center" m={4}>
          Registartion Form
        </Typography>
      </div>
      <Box
        component="form"
        sx={{
          "& .MuiTextField-root": { m: 1, width: "50%" },
        }}
        noValidate
        autoComplete="off"
      >
        {/* {!isPassword && (
          <Typography color="red" variant="h6" align="center" m={4}>
            Password not match
          </Typography>
        )} */}

        <div style={{ display: "flex" }}>
          <TextField
            label="Enter your email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            type="password"
            label="Enter your password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <TextField
            type="password"
            label="Enter your confirm password"
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
          />
        </div>
      </Box>
      <div style={{ display: "flex", justifyContent: "space-around" }}>
        <Link style={{ textDecoration: "none" }} to={"/"}>
          <Button onClick={handleCreateUser} buttonText="Register" />
        </Link>
        <Link style={{ textDecoration: "none" }} to={"/login"}>
          <Button onClick={handleCreateUser} buttonText="Log In" color="warning" />
        </Link>
        <Button onClick={goBack} buttonText="Cancel" color="error" />
      </div>
    </>
  );
};

export default RegistrationForm;
