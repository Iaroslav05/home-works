import { v4 as uuidv4 } from "uuid";

enum CurrencyEnum {
  USD = "USD",
  UAH = "UAH",
}

interface Icard {
  listTransaction?: Transaction[];
  addTransaction(transaction: Transaction): string;
  addTransaction(currency: CurrencyEnum, amount: number): string;
  getTransaction(id: string): Transaction | undefined;
  getBalance(currency: CurrencyEnum): number;
}

class Transaction {
  id: string = uuidv4();
  amount: number;
  currency: CurrencyEnum;

  constructor(amount: number, currency: CurrencyEnum) {
    this.amount = amount;
    this.currency = currency;
  }
}

class Card implements Icard {
  listTransaction: Transaction[] = [];

  addTransaction(transaction: Transaction): string;
  addTransaction(currency: CurrencyEnum, amount: number): string;

  addTransaction(
    transactionOrCurrency: Transaction | CurrencyEnum,
    amount?: number
  ): string {
    if (transactionOrCurrency instanceof Transaction) {
      this.listTransaction.push(transactionOrCurrency);
      return transactionOrCurrency.id;
    } else if (transactionOrCurrency && amount !== undefined && amount > 0) {
      const transaction = new Transaction(amount, transactionOrCurrency);
      this.listTransaction.push(transaction);
      return transaction.id;
    } else {
      throw new Error("Invalid arguments");
    }
  }

  getTransaction(id: string): Transaction | undefined {
    if (this.listTransaction && id) {
      return this.listTransaction.find((transaction) => transaction.id === id);
    }
  }

  getBalance(currency: CurrencyEnum): number {
    return this.listTransaction.reduce((accum, transaction) => {
      if (transaction.currency === currency) {
        return accum + transaction.amount;
      } else {
        return accum;
      }
    }, 0);
  }
}

class BonusCard extends Card {
  percent: number;
  constructor(num: number) {
    super();
    this.percent = num / 100;
  }
  addTransaction(
    transactionOrCurrency: Transaction | CurrencyEnum,
    amount?: number
  ): string {
    if (transactionOrCurrency instanceof Transaction) {
      this.listTransaction.push(transactionOrCurrency);
      return transactionOrCurrency.id;
    } else if (transactionOrCurrency && amount !== undefined && amount > 0) {
      const transaction = new Transaction(amount, transactionOrCurrency);
      this.listTransaction.push(transaction);
      const bonusAmount = transaction.amount * this.percent;
      const bonusTransaction = new Transaction(
        bonusAmount,
        transactionOrCurrency
      );
      this.listTransaction.push(bonusTransaction);
      return transaction.id;
    } else {
      throw new Error("Invalid arguments");
    }
  }
}

class Pocket {
  listCard: { [name: string]: Icard };
  constructor() {
      this.listCard = {};
  }

  addCard(name: string, card: Card): void {
    this.listCard[name] = card;
  }

  removeCard(name: string): void {
    delete this.listCard[name];
  }

  getCard(name: string) {
    return this.listCard[name];
  }

  getTotalAmount(currency: CurrencyEnum) {
    const card = Object.values(this.listCard);
    return card.reduce((accum, transaction) => {
      return accum + transaction.getBalance(currency);
    }, 0);
  }
}

const card = new Card();
const transactionUsd = card.addTransaction(
  new Transaction(100, CurrencyEnum.USD)
);
card.addTransaction(CurrencyEnum.USD, 50);
card.addTransaction(CurrencyEnum.USD, 150);

console.log(transactionUsd);
console.log(card.getBalance(CurrencyEnum.USD));

// const transactionUah = card.addTransaction(
//   new Transaction(300, CurrencyEnum.UAH)
// );
// card.addTransaction(CurrencyEnum.UAH, 150);
// card.addTransaction(CurrencyEnum.UAH, 200);
// console.log(transactionUah);
// console.log(card.getBalance(CurrencyEnum.UAH));

const bonus = new BonusCard(10);
const bonusTransactionUah = bonus.addTransaction(
  new Transaction(300, CurrencyEnum.UAH)
);
bonus.addTransaction(CurrencyEnum.UAH, 150);
bonus.addTransaction(CurrencyEnum.UAH, 200);
console.log(bonusTransactionUah);

// console.log(bonus.getBalance(CurrencyEnum.UAH));
bonus.addTransaction(CurrencyEnum.USD, 200);
console.log(bonus.getBalance(CurrencyEnum.USD));

const pokedCard = new Pocket();
pokedCard.addCard("card", card);
pokedCard.addCard("bonus", bonus);
// pokedCard.removeCard("bonus");
// console.log(JSON.stringify(pokedCard.getCard("card")));
// console.log(pokedCard.getTotalAmount(CurrencyEnum.UAH));
console.log(pokedCard.getTotalAmount(CurrencyEnum.USD));
// console.log(JSON.stringify(pokedCard.listCard, null, 2));

export { CurrencyEnum, Card, Pocket };
