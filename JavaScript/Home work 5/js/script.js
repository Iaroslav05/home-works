"use strict";

function createNewUser() {

	let firstName = prompt('Enter your first name');
	let lastName = prompt('Enter your last name');

	let newUser = {
		firstName,
		lastName,

		fullName() {
			return `${this.firstName} ${this.lastName}`;
		},

		getLogin() {
			return (this.firstName.charAt(0) + this.lastName).toLowerCase();
		},
		setFirstName(newName) {
			Object.defineProperty(newUser, 'firstName', { value: newName, })
		},
		setLastName(newName) {
			Object.defineProperty(newUser, 'lastName', { value: newName, })
		}

	}
	Object.defineProperties(newUser, {
		firstName: { writable: false },
		lastName: { writable: false },
	})

	return newUser;
}

const user = createNewUser();

console.log(user.fullName());
console.log(user.getLogin());
console.log(user);

user.setFirstName('Іван')
user.setLastName('Підкова')

console.log(user);
console.log(user.fullName());
console.log(user.getLogin());
