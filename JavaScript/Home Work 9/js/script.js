const arr = ['Kiev', 'Lviv', 'Rivne', 'Lutsk', 'Khmelnytskyi', 'Zhitomir', ['Volyn', 'Stepan']];
const div = document.createElement('div');
const divCounter = document.createElement('div');
divCounter.classList.add('counter-wrapper');
let p = document.createElement('p');
	p.classList.add('counter');
	divCounter.append(p);

let list = arr.map(function (item, index){
	if (Array.isArray(item)){
		return `<ul>${item.map((item2) => {
			return `<li>${item2}</li>`
		}).join('')}</ul>`
	}else{
		return `<li>${item}</li>`
	}
}).join('');
div.innerHTML = list;

let timer;
let counter = 3;
function timers() {
document.querySelector('counter')
p.innerText = (`Залишилося ${counter}: секунд`);
counter--;
if (counter < 0) {
div.remove();
} else {
timer = setTimeout(timers, 1000);
}
}
timers();

document.body.prepend(div);
document.body.prepend(divCounter);
