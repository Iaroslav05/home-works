"use strict";

const tabs = document.querySelector('.tabs');
const childsTab = tabs.children;

const tabsContent = document.querySelector('.tabs-content');
const childsTabContent = tabsContent.children;



for (const childTab of childsTab) {
	childTab.addEventListener('click', function () {

		for (const childTab of childsTab) {
			childTab.classList.remove('active');
		};
		childTab.classList.add('active');

		for (const childTabContent of childsTabContent) {

			childTabContent.classList.remove('active');
		};

		const tabName = childTab.textContent;

		document.querySelector(`[data-name = '${tabName}']`).classList.add('active');
		console.log(tabName);

	});
	console.log(childTab);

}

// for (let i = 0; i < childsTabContent.length; i++) {
// 	let value = i + 1;
// 	childsTabContent[i].dataset.link = value;
// 	console.log(childsTabContent[i]);
// }

// for (const tabContent of childTabsContents) {
// 	tabContent.dataset.link = value;
// 	console.log(value);
// 	console.log(tabContent);
// }

