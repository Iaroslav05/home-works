"use strict";

let paragraphs = document.querySelectorAll('p');
for (let paragraph of paragraphs) {
	paragraph.style.backgroundColor = '#ff0000';
}


let list = document.querySelector('#optionsList');
console.log(list);

if (list.hasChildNodes()) {
	console.log(list.hasChildNodes());

	let listChildrens = list.childNodes;
	console.log(listChildrens);
	for (let listChildren of listChildrens) {
		console.log(listChildren.nodeName);
		console.log(listChildren.nodeType);
	}
}


let parentList = document.querySelector('#optionsList');
console.log(parentList.parentNode);

let testParagraph = document.querySelector('#testParagraph');
testParagraph.innerHTML = `This is a paragraph`;
console.log(testParagraph.innerHTML);
console.log(testParagraph);


let header = document.querySelector('.main-header');
console.log(header);

if (header.hasChildNodes()) {
	console.log(header.hasChildNodes())
}

let headerChilds = header.children;
console.log(headerChilds);

for (let headerChild of headerChilds) {
	headerChild.classList.add('nav-item');
	console.log(headerChild.className);
}


let sectionTitle = document.querySelector('.section-title');
sectionTitle.classList.remove('section-title');
console.log(sectionTitle.classList);


