"use strict";

function createNewUser() {

	let firstName = prompt('Enter your first name');
	let lastName = prompt('Enter your last name');
	let birthday = prompt('Enter your birthday date (dd.mm.yyyy)');	

	let newUser = {
		firstName,
		lastName,
		birthday,

		fullName() {
			return `${this.firstName} ${this.lastName}`;
		},

		getAge() {
	let today = new Date();
	birthday = new Date(birthday);
	let age = today.getFullYear() - birthday.getFullYear();
	return age;
		},

		getLogin() {
			return (this.firstName.charAt(0) + this.lastName).toLowerCase();
		},

		getPassword(){
			return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
		}

}
return newUser;
}

const user = createNewUser();
console.log(user.fullName());
console.log('age:' + ' ' + user.getAge());
console.log(user.getLogin());
console.log(user.getPassword());
console.log(user);

