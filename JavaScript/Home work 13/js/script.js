"use strict";

const stopButton = document.querySelector('.slaider-stop');
const startButton = document.querySelector('.slaider-start')
const slaider = document.querySelector('.slaider');
let offSet = 0
let timer;

function autoSlider() {
	timer = setTimeout(function showSlaider() {
		offSet += 400;
		if (offSet > 1200) {
			offSet = 0;
		}
		slaider.style.left = -offSet + 'px';
		autoSlider();
	}, 3000);
}
autoSlider();

startButton.addEventListener('click', startSlaider);

function startSlaider() {
	startButton.setAttribute('disabled', true);

	autoSlider();
}

stopButton.addEventListener('click', stopSlaider);

function stopSlaider() {
	clearTimeout(timer);
}


function stopSlaider() {
	startButton.removeAttribute('disabled');
	clearTimeout(timer);
}



