// let array: Array<string>;
let array: string[];
array = ["1", "2", "3", "4"];

// не дасть змінити значення в масиві
const numbers: ReadonlyArray<number> = [1, 2, 3, 4];
// numbers.push() помилка
// numbers[0] = 8 помилка тільки для читання;

// Кортежи прописуєм які типи значень і їх порядок

type TypeArr = [number, string, null];
const arr: TypeArr = [1, "2", null];
