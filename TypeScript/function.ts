type TypeNameReturn = {
  name: string;
};

// function getName(name: string): TypeNameReturn(що має повернути функція) {
//   return { name };
// }

function getName(name: string): TypeNameReturn {
  return { name };
}
getName("Yaroslav");

// описуєм стрілкову функцію

type TypeFirstNameFunction = (name: string) => TypeNameReturn;

const getFirstName: TypeFirstNameFunction = (name) => {
  return { name };
};
getName("Yaroslav");

// rest

const getNum = (...num: number[]) => {
  return num;
};

// функціональні перегрузки
function getCar(name: string): string;
function getCar(name: string, price: number): string;

function getCar(name: string, price?: number): string {
  return price ? `Марка ${name}: ціна${price}` : `Марка ${name}`;
}

const carOne = getCar("BMW");
const carTwo = getCar("BMW", 300);
// const carThree = getCar("BMW", 300, M3); помилка потрібно один або два аргументи
