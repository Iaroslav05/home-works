//  assertions це коли тип змінної не визначений і ми задаєм його примусово
const inputElement = document.querySelector("input");
// const inputValue1 = inputElement?.value; так буде по замовчуванню
const inputValue1 = (inputElement as HTMLInputElement).value;
// або
const inputValue2 = (<HTMLInputElement>inputElement).value;

// Not null assertions (!) означає що тип змінної яка повертається не буде null and undefined
const getLength = (text: string | null) => {
  return text!.length;
};

getLength("wrapper");
// getLength(null)'' тут буде помилка при компіляції бо не повино бути null
