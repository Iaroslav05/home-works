// розширені типи
type TypeIsNumber<T> = T extends number ? "yes" : "no";

type Type1 = TypeIsNumber<number>;
type Type2 = TypeIsNumber<string>;

type TypeBrand = "BMW" | "Audi" | "VW";
type TypePrice = "$15000" | "$10000" | "$5000";
type TypeCar = `${TypeBrand} ${TypePrice}`;

// в "" натиснути ctrl + space
const carGermany: TypeCar = "BMW $10000";
