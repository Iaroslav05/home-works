// generik function
function entity<T>(arg: T): T {
  return arg;
}
entity<number>(1);
entity<string>("1");

const entity2 = <T>(arg: T): T => {
  return arg;
};

// generik Class

class Channel<T> {
  private name: T;

  constructor(name: T) {
    this.name = name;
  }

  getName(): T {
    return this.name;
  }
}

new Channel<string>("Rivne");
new Channel<number>(2);

// generic interface
interface IPair<K, V> {
  key: K;
  value: V;
}

const pair: IPair<string, number> = {
  key: "Rivne",
  value: 1,
};
const pair1: IPair<number, number> = {
  key: 0,
  value: 1,
};
// Задати тип generic по замовчуванні
type TypeNameLength = {
  length: number;
};

function getNameLength<T extends TypeNameLength>(entity: T): number {
  return entity.length;
}

getNameLength("Rivne");
getNameLength([1, 2, 2, "5"]);
