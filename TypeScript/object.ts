// оєднення типів за допоиогою рест оператора
type TypeUser = {
  name: string;
  age: number;
};

let user: TypeUser;

user = {
  name: "Yaroslav",
  age: 38,
};

type TypeAddress = {
  address: string;
};

let address: TypeAddress;

address = {
  address: "Rivne",
};

let infoUser: TypeUser & TypeAddress;

infoUser = {
  ...user,
  ...address,
};
