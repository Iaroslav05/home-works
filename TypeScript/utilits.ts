// Pick, Omit, Partial
interface ICar {
  id: number;
  name: string;
  price?: number;
  yearBuilt: number;
}

// при розниренні нам не потрібний ключ id
interface ICarCreate extends Omit<ICar, "id"> {}
const car: ICarCreate = {
  name: "BMW",
  price: 1000,
  yearBuilt: 2021,
};

// якщо треба якийсь один ключ
interface ICarId extends Pick<ICar, "id"> {}
const carId: ICarId = {
  id: 258,
};

// робить всі ключі не обовязковими
interface ICarOptions extends Partial<ICar> {}
const carOptions: ICarOptions = {
  yearBuilt: 2021,
};

// тільки для читання змінювати не дасть помилка буде
interface ICarReadOnly extends Readonly<ICar> {}

// для створення свого типу
type TypeCarRecord = Record<"name" | "price", string | number>;

const carRecord: TypeCarRecord = {
  name: 2021,
  price: "2222",
};

// робить всі поля обовязковими
interface ICarRequired extends Required<ICar> {}

// function показує що ми виводими returnType
type TypeGetName = () => string;
type Return = ReturnType<TypeGetName>;

// Специфіка поверне те що повторюється "Slavik"
type TypeName = Extract<"Yaroslav" | "Slavik", "Slavik" | "Yura">;

//виключає по першому параметрі буде "Yaroslav"
type TypeName1 = Exclude<"Yaroslav" | "Slavik", "Slavik" | "Yura">;

// видаляє всі null and undefined
type NotNullAndUndefined = NonNullable<string | null | undefined>;
