interface IUser {
  name: string;
  pass: string;
  email: string;
}

interface IUserAge {
  age: number;
}

const userInfo: IUser = {
  name: "Yaroslav",
  pass: "12345678",
  email: "some@i.ua",
};

// розширення інтерфейсів
interface IUserAge {
  age: number;
}

interface IUserInfo extends IUserAge {
  name: string;
}

const person: IUserInfo = {
  name: "Yaroslav",
  age: 39,
};
