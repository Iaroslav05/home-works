class Car {
  name: string;
  price: number;
  constructor(name: string, price: number) {
    this.name = name;
    this.price = price;
  }

  getInfoCar(): string {
    return `${this.name} - ${this.price}`;
  }

  //   приватний метод # або private визивається тільки в класі
  #infoCar(): string {
    return `${this.name} - ${this.price}`;
  }
  // працює так же як і prived но його можна викликати в класі який розширюється від цього класу
  protected infoCar(): string {
    return `${this.name} - ${this.price}`;
  }

  anything() {
    this.#infoCar();
  }
}

class Bus extends Car {
  category: string;
  constructor(name: string, price: number, category: string) {
    super(name, price);
    this.category = category;
  }

  getBus() {
    this.infoCar();
  }
}

new Car("BMW", 1000).getInfoCar();
new Bus("Mercedes", 2000, "bus");
