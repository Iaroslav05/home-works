// Це опис якихось констант
enum EnumRoles {
  ADMIN,
  USER,
  GUEST,
}

interface IUserRole {
  role: EnumRoles;
  color: EnumColors;
}

//  прописуєм enum через const enum для покращення оптимізації
const enum EnumColors {
  black,
  pink,
  green,
}

const userRole: IUserRole = {
  role: EnumRoles.ADMIN,
  color: EnumColors.green,
};
