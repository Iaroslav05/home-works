// декоратор це функція яка накладує свій функціонал на Ваші клас метод свойства ....
function LogClass(constructor: Function) {
  console.log(constructor.name);
}

function LogProp(target: Object, key: string | Symbol) {
  console.log(key);
}

function LogMethod(
  target: Object,
  key: string | Symbol,
  descriptor: PropertyDescriptor
) {
  console.log(descriptor);
}

@LogClass
class Plane {
  @LogProp
  private id: number;

  constructor(id: number) {
    this.id = id;
  }

  @LogMethod
  getId() {
    return this.id;
  }

  @LogMethod
  get Id() {
    return this.id;
  }
}

// Практика==========================================
interface ComponentDecorator {
  selector: string;
  template: string;
}

function Component(config: ComponentDecorator) {
  return function <T extends { new (...arg: any[]): object }>(Constructor: T) {
    return class extends Constructor {
      constructor(...arg: any[]) {
        super(...arg);

        const elem = document.querySelector(config.selector)!;
        elem.innerHTML = config.template;
      }
    };
  };
}

function Bind(
  _: any,
  _2: any,
  descriptor: PropertyDescriptor
): PropertyDescriptor {
  const originalFn = descriptor.value;

  return {
    configurable: true,
    enumerable: false,
    get() {
      return originalFn.bind(this);
    },
  };
}

@Component({
  selector: "#card",
  template: `
    <div class="card">
      <span>Card component</span>
    </div>
  `,
})
class CardComponent {
  name: string;

  constructor(name: string) {
    this.name = name;
  }

  @Bind
  logName(): void {
    console.log(`Component name: ${this.name}`);
  }
}

const card = new CardComponent("My card");

const btn = document.querySelector("#btn")!;
btn.addEventListener("click", card.logName);

// ===============================================================
type ValidatorType = "required" | "email" | "pass";

interface IValidatorConfig {
  [prop: string]: {
    [validateEmail: string]: ValidatorType;
    [validatePass: number]: ValidatorType;
  };
}

const validators: IValidatorConfig = {};

function Required(target: any, propName: string) {
  validators[target.constructor.name] = {
    ...validators[target.constructor.name],
    [propName]: "required",
  };
}

function validate(obj: any): boolean {
  const objectConfig = validators[obj.constructor.name];
  if (!objectConfig) {
    return true;
  }
  let isValid = true;
  Object.keys(objectConfig).forEach((key) => {
    if (objectConfig[key] === "required") {
      isValid = isValid && !!obj[key];
    }
    return isValid;
  });
}

class Form {
  @Required
  public email: string | void;
  public pass: number | void;

  constructor(email?: string, pass?: number) {
    this.email = email;
    this.pass = pass;
  }
}

const form = new Form("example@i.oa", 1234);
if (validate(form)) {
  console.log("Valid:", form);
} else {
  console.log("Validate Error");
}
