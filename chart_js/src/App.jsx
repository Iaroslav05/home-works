import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Typography } from '@mui/material';

import Dashboard from "./components/Dashboard/Dashboard.jsx"
import {fetchDataChart} from "./store/reducers/dataCharsSlice.js";
import { selectCoins } from "./store/actions/action.js";

const App = () => {
		const dispatch = useDispatch()
		const { dataChart, loading } = useSelector(selectCoins);

		useEffect( () => {
				dispatch(fetchDataChart())
		}, [dispatch] );
		

  return (
   <div>
   <Typography variant="h3" align ="center">Hello Charts</Typography>
	 {loading ?
		 <div>Loading.... </div>
		 :
		 <Dashboard dataChart={dataChart}/>
		}
  
   </div>
  )
}

export default App
