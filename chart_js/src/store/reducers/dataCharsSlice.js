import axios from "axios";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const initialState = {
		dataChart: [], 
		loading: false,
		error: null
}

const baseUrl = "https://api.coinranking.com/v2/coins"
const apiKey = "coinrankingb8dc4cb407e1084548d2cc8ff4803cb4f9ad0ce902d2c8ce"

export const fetchDataChart = createAsyncThunk (
		"dataChart/fetchDataChart", async ( thunkAPI) => {
				try {
						const response = await axios.get(`${baseUrl}/?limit=10`, {
								headers: {
										Authorization: `Bearer ${ apiKey }`,
								},
						})
						return response.data
				}
				catch (error ){
						return thunkAPI.rejectWithValue(error.message);
				}
		}
)

export const dataChartSlice = createSlice({
		name: "dataChart",
		initialState,
		reducers: {	},
		extraReducers: (builder) => {
				builder
					.addCase(fetchDataChart.pending, (state) => {
							state.loading = true;
							state.error = null;
					})
					.addCase(fetchDataChart.fulfilled, (state, action) => {
							state.loading = false;
							state.dataChart = action.payload;
					})
					.addCase(fetchDataChart.rejected, (state, action) => {
							state.loading = false;
							state.error = action.error.message;
					});
		},
});

export default dataChartSlice.reducer;