import {configureStore} from "@reduxjs/toolkit";

import dataChartReducer from "./reducers/dataCharsSlice.js"

export const store = configureStore({
		reducer: {
				dataChart: dataChartReducer
		},
})