import PropTypes from "prop-types";
import { styled } from '@mui/material/styles';
import { Container, Box, Paper } from '@mui/material';
import Grid from '@mui/material/Unstable_Grid2';

import BarChart from "../charts/BarChart/BarChart.jsx";
import Doughnut from "../charts/Doughnut/Doughnut.jsx";
import LineChart from "../charts/LineChart/LineChart.jsx";
import PieChart from "../charts/PieChart/PieChart.jsx";

const Item = styled(Paper)(({ theme }) => ({
		backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
		...theme.typography.body2,
		padding: theme.spacing(1),
		textAlign: 'center',
		color: theme.palette.text.secondary,
}));

const Dashboard = ({dataChart}) => {
		return (
			<Container maxWidth="lg">
			<Box sx={{ width: '100%' }}>
			<Grid container rowSpacing={3} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
					<Grid xs={12} sm={6} lg={6}>
							<Item>
									<BarChart dataChart={dataChart}/>
							</Item>
					</Grid>
					<Grid xs={12} sm={6} lg={6} >
							<Item>
									<LineChart dataChart={dataChart}/>
							</Item>
					</Grid>
					<Grid xs={12} sm={6} lg={6}>
							<Item>
									<PieChart dataChart={dataChart}/>
							</Item>
					</Grid>
					<Grid xs={12} sm={6} lg={6}>
							<Item>
									<Doughnut dataChart={dataChart}/>
							</Item>
					</Grid>
			</Grid>
			</Box>
			</Container>
		);
};

export default Dashboard;

Dashboard.propTypes ={
		dataChart: PropTypes.array
}