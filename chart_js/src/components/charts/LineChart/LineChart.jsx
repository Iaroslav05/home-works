import PropTypes from "prop-types";
import { Chart as ChartJS, LineElement, PointElement, CategoryScale} from "chart.js";
import {Line} from "react-chartjs-2"

ChartJS.register(
	CategoryScale,
	LineElement,
	PointElement
	)
const LineChart = ({dataChart}) => {
			let data = {
				labels:  dataChart.data?.coins.map(x => x.name),
				datasets: [{
						label: `${dataChart.data?.coins.length} Coins Available`,
						data: dataChart.data?.coins.map(y => y.price ),
						backgroundColor: dataChart.data?.coins.map(y => y.color ),
						borderColor:dataChart.data?.coins.map(y => y.color ),
						borderWidth: 1
				}]
		}
		
		let options = {
				scales: {
						y: {
								beginAtZero: true
						}
				},
		}
		
		return (
			<div>
					<Line data={data} options={options}/>
			</div>
		);
};

export default LineChart;

LineChart.propTypes ={
		dataChart: PropTypes.array
}