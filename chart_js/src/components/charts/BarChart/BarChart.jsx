import PropTypes from "prop-types";
import { Chart as ChartJS, BarElement, CategoryScale, LinearScale} from "chart.js";
import {Bar} from "react-chartjs-2"

ChartJS.register(
	CategoryScale,
	LinearScale,
	BarElement
)
const BarChart = ({ dataChart }) => {
			let data = {
				labels:  dataChart.data?.coins.map(x => x.name),
					datasets: [{
						label: `${dataChart.data?.coins.length} Coins Available`,
						data: dataChart.data?.coins.map(y => y.price ),
							backgroundColor: dataChart.data?.coins.map(y => y.color ),
							borderColor:dataChart.data?.coins.map(y => y.color ),
							borderWidth: 1
					}]
		}
		
		let options = {
				scales: {
						y: {
								beginAtZero: true
						}
				},
		}
	
		return (
			<div>
					<Bar data={data} options={options}/>
			</div>
		);
};

export default BarChart;

BarChart.propTypes ={
		dataChart: PropTypes.array
}