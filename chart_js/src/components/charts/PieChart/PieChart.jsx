import PropTypes from 'prop-types';
import { Chart as ChartJS, ArcElement, Tooltip, Legend} from "chart.js";
import {Pie} from "react-chartjs-2"

ChartJS.register(
	Legend,
	ArcElement,
	Tooltip,
	Legend
)
const PieChart = ({dataChart}) => {
			let data = {
				labels:  dataChart.data?.coins.map(x => x.name),
				datasets: [{
						label: `${dataChart.data?.coins.length} Coins Available`,
						data: dataChart.data?.coins.map(y => y.price ),
						backgroundColor: dataChart.data?.coins.map(y => y.color ),
						borderColor:dataChart.data?.coins.map(y => y.color ),
						borderWidth: 1
				}]
		}
		
		let options = {
				scales: {
						y: {
								beginAtZero: true
						}
				},
				legend: {
						labels: {
								fontSize: 25,
						},
				},
				
		}
		
		return (
			<div>
					<Pie data={data} options={options}/>
			</div>
		);
};

export default PieChart;

PieChart.propTypes ={
		dataChart: PropTypes.array
}