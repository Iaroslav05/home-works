// itgid.info - курс Node.js

// Создайте анонимный модуль в данном файле который с помощью модуля task_01 делает запрос в бд и возвращает максимальную цену на автомобиль в таблице

// не получается - спроси
// показано на примере db auto и таблицы cars
// для запроса используем пакет sync-mysql

const syncMysql         = require('sync-mysql');
const CONFIG            = require('./config');
const task_01 = require('./task_01');

module.exports = function () {
   try {
     const query = "SELECT MAX(cost) AS cost FROM cars";

     if (typeof query !== "string") {
       throw new Error("Query must be a string");
     }

     const result = task_01(query);

     return result[0].cost;
     
   } catch (error) {
     console.error("Error executing query:", error.message);
     throw new Error("Database query failed");
   } 
}