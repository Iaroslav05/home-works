// itgid.info - курс Node.js

// Создайте анонимный модуль в данном файле который с помощью модуля task_01 делает запрос в бд и возвращает объект, где ключ - цвет автомобиля, а значение - количество автомобилей в базе с таким цветом.

// постарайтесь уникальность выборки реализовать через запрос
// не получается - спроси
// показано на примере db auto и таблицы cars
// для запроса используем пакет sync-mysql

const syncMysql         = require('sync-mysql');
const CONFIG            = require('./config');
const task_01 = require('./task_01');

module.exports = function () {
   try {
     const query ="SELECT color, COUNT(*) AS count FROM cars GROUP BY color";

     if (typeof query !== "string") {
       throw new Error("Query must be a string");
     }

     const result = task_01(query);

     const colorCounts = {};

     result.forEach((row) => {
       colorCounts[row.color] = row.count;
     });

     return colorCounts;

   } catch (error) {
     console.error("Error executing query:", error.message);
     throw new Error("Database query failed");
   } 
}