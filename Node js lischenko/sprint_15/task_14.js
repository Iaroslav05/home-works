// itgid.info - курс Node.js

// Создайте анонимный модуль в данном файле который с помощью модуля task_01 делает запрос в бд и возвращает массив моделей автомобилей 2015 и 2017 года выпуска


// постарайтесь выборку одной записи реализовать через запрос
// не получается - спроси
// показано на примере db auto и таблицы cars
// для запроса используем пакет sync-mysql

const syncMysql         = require('sync-mysql');
const CONFIG            = require('./config');
const task_01 = require('./task_01');

module.exports = function () {
  try {
    const query = "SELECT * FROM cars WHERE year = 2015 OR year = 2017";

    if (typeof query !== "string") {
      throw new Error("Query must be a string");
    }

    const result = task_01(query);

    return result;
  } catch (error) {
    console.error("Error executing query:", error.message);
    throw new Error("Database query failed");
  }
}

