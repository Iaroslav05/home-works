// itgid.info - курс Node.js

// Создайте анонимный модуль в данном файле который с помощью модуля task_01 делает запрос в бд и возвращает массив уникальных color автомобилей

// т.е. осуществляет выборку с помощью task_01 только уникальных color, а потом методами массива дорабатывает результат.
// постарайтесь уникальность выборки реализовать через запрос
// показано на примере db auto и таблицы cars
// для запроса используем пакет sync-mysql

const syncMysql         = require('sync-mysql');
const CONFIG            = require('./config');
const task_01 = require('./task_01');

module.exports = function () {
   try {
     const query = "SELECT DISTINCT color FROM cars;";

     if (typeof query !== "string") {
       throw new Error("Query must be a string");
     }

     const result = task_01(query);

     return result.map((item) => item.color);
   } catch (error) {
     console.error("Error executing query:", error.message);
     throw new Error("Database query failed");
   } 
}