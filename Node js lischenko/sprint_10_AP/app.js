const http = require("http");
const fs = require("fs");
const path = require("path");
const url = require("url");

const { mimeTypes } = require("./modules/mimeTypes");
const { staticFile, fineUser, parseCookies } = require("./modules/functions");
const users = require("./modules/users");

const PORT = 3500;

let cookie;

http
  .createServer((req, res) => {
    let url = req.url;
    url = url.split("?")[0];
    console.log(url);

    switch (url) {
      case "/":
        console.log("main page");
        staticFile(res, "/html/main.html", ".html");
        break;

      case "/about":
        console.log("about page");
        staticFile(res, "/html/about.html", ".html");
        break;

      case "/contact":
        console.log("contact page");
        staticFile(res, "/html/contact.html", ".html");
        break;

      case "/admin":
        console.log("admin page");
        cookie = parseCookies(req);
        console.log(cookie);

        if (cookie.uid in users) {
          staticFile(res, "/html/admin.html", ".html");
        } else {
          staticFile(res, "/html/not_admin.html", ".html");
        }
        break;

      case "/login":
        console.log("login page");
        cookie = parseCookies(req);
        if ("uid" in cookie) {
          res.setHeader("Set-Cookie", ["uid='';max-age=-1", "u=;max-age=0"]);
          staticFile(res, "/html/exit.html", ".html");
        } else {
          staticFile(res, "/html/login.html", ".html");
        }
        break;

      case "/cabinet":
        console.log("cabinet page");
        const current_url = new URL(`http://localhost:3500${req.url}`);
        const search_params = current_url.searchParams;
        const login = search_params.get("login");
        const password = search_params.get("password");
        console.log(login, password);

        const uid = fineUser(login, password);
        if (uid) {
          res.setHeader("Set-Cookie", [
            "uid=" + uid,
            "u=" + users[uid]["name"],
          ]);
          staticFile(res, "/html/cabinet.html", ".html");
        } else {
          res.writeHead(200, {
            "Set-Cookie": ["uid=;max-age 0", "u=;max-age 0"],
            "Content-Type": "text/plain",
          });
        }

        break;

      default:
        const extname = String(path.extname(url)).toLocaleLowerCase();
        if (extname in mimeTypes) staticFile(res, url, extname);
        else {
          res.statusCode = 404;
          res.end();
        }
    }
  })
  .listen(PORT);
