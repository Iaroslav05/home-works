const { mimeTypes } = require("../mimeTypes");
const fs = require("fs");

module.exports.staticFile = function (res, filePath, ext) {
  res.setHeader("Content-Type", mimeTypes[ext]);

  fs.readFile("./public" + filePath, (error, data) => {
    if (error) {
      res.statusCode(error);
      res.end;
    }
    res.end(data);
  });
};
