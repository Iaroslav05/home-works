const { staticFile } = require("./staticFile");
const { fineUser } = require("./findUser");
const { parseCookies } = require("./parseCookies")

module.exports = {
  staticFile: staticFile,
  fineUser: fineUser,
  parseCookies: parseCookies,
};
