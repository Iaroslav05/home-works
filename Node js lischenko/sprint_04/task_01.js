// itgid.info - курс Node.js


// Task 01
// Создайте анонимный модуль в данном файле, который принимает аргумент - путь к файлу. Возвращает содержимое файла. Пример вызова в index.js

const fs = require('fs');

module.exports = function (path) {
return  fs.readFileSync(path, {encoding: "utf8", flag: "r"})
}

