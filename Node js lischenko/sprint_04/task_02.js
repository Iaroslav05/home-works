// itgid.info - курс Node.js


// Task 02
// Создайте анонимный модуль в данном файле, который принимает аргумент - путь к файлу. Возвращает сумму чисел из файла. Пример вызова в index.js

const fs = require( "fs" );

module.exports = function ( path ) {
		const data = fs.readFileSync( path, { encoding: "utf8", flag: "r" } );
		let dataArr = data.trim().split( "\r\n" );
		return dataArr.reduce((accum, item) => (+accum) + (+item))
		
		
};

