// itgid.info - курс Node.js


// Task 03
// Создайте анонимный модуль в данном файле, который принимает аргумент - путь к файлу. Возвращает массив слов из первой строки файла. Разбиение производить по символу пробел. Пример вызова в index.js

const fs = require( "fs" );

module.exports = function ( path ) {
		const data = fs.readFileSync( path, { encoding: "utf8", flag: "r" } );
		return  data.trim().split(" ");
		
		
};

