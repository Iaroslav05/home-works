const t01 = require("./task_01");
const t02 = require("./task_02");
const t03 = require("./task_03");
const t04 = require("./task_04");
const t05 = require("./task_05");
const t06 = require("./task_06");
const t07 = require("./task_07");
const t08 = require("./task_08");
const t09 = require("./task_09");
const t10 = require("./task_10");
// остальные require допишите по аналогии

// console.log("task 1");
// console.log(t01("./d_04/noks.txt"));

// console.log('task 2');
// console.log(t02('./d_04/tost.txt'));

// console.log('task 3');
// console.log(t03('./d_04/words.txt'));

// console.log('task 4');
// console.log(t04('./d_04/code.dat'));
//
// console.log('task 5');
// console.log(t05('./d_04/dubl.txt'));

console.log("task 6");
t06("this is\r\ntask 6");

console.log("task 7");
t07("primavera");

console.log("task 8");
const arr_08 = ["The Sims", "Diablo 2", "Baldurs Gate 2"];
t08(arr_08);

console.log("task 9");
const arr_09 = [
  [1, 0, 1, 0],
  [0, 1, 0, 1],
  [1, 0, 1, 0],
  [0, 1, 0, 1],
];
t09(arr_09);

// console.log("task 10");
// t10("d_04/f_01.txt", "d_04/f_02.txt");
