// itgid.info - курс Node.js


// Task 04
// Создайте анонимный модуль в данном файле, который принимает аргумент - путь к файлу. Файл содержит строку код. Пусть модуль возвращает true, если в коде есть число 7 и false в остальных случаях. Пример вызова в index.js

const fs = require('fs');

module.exports = function (path) {
const data = fs.readFileSync(path, {encoding: "utf8", flag: "r"})
		return data.includes("7")
}

