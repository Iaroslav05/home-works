// itgid.info - курс Node.js

// Task 06
// Создайте анонимный модуль в данном файле, который принимает аргумент - текст. Модуль должен создать файл с именем t_06.txt в папке d_04. В файл должен быть записан текст. Кодировка utf8. Флаг файловой системы - w.

const fs = require("fs");
const path = require("path");

module.exports = function (text) {
  const pathToFile = "d_04/t_06.txt";
  return fs.writeFileSync(path.join(__dirname, pathToFile), text, {
    encoding: "utf8",
    flag: "w",
  });
};
