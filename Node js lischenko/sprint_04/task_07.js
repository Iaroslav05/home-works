// itgid.info - курс Node.js

// Task 07
// Создайте анонимный модуль в данном файле, который принимает аргумент - строку. Модуль должен записать строку в файл t_07.dat в папке d_04, причем после каждого символа строки должен быть пробел. Кодировка utf8. Флаг файловой системы - w.

// например строка 'abba' в файле должен быть текст
//a b b a

const fs = require("fs");
const path = require("path");

module.exports = function (s) {
  const text = s.trim().split("").join(" ");
  const pathToFile = "d_04/t_07.dat";
  return fs.writeFileSync(path.join(__dirname, pathToFile), text, {
    encoding: "utf8",
    flag: "w",
  });
};
