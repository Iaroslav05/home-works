// itgid.info - курс Node.js


// Task 05
// Создайте анонимный модуль в данном файле, который принимает аргумент - путь к файлу. Читает файл в переменную. Текст содержит множество двойных пробелов. Замените их все на одинарные и возвратите строку. 

const fs = require('fs');

module.exports = function (path) {
		const data = fs.readFileSync( path, { encoding: "utf8", flag: "r" } );
		return data.replaceAll( "  ", " " )
}

