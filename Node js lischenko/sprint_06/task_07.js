// itgid.info - курс Node.js 2024

// Task 07.
// Создайте анонимный модуль, который помощью модуля getCSV читает файл ./f_06/v_07.csv, и возвращает сумму всех чисел файла. Пример вызова модуля в index.js. 

const getCSV = require('./get_csv');

module.exports = async function () {
const records = await getCSV("./f_06/v_07.csv", {delimiter: ";"});
let sum = records.flat().reduce((accum, item) => {
	if (!isNaN(item)) {
    return accum + parseFloat(item);
  }
  return accum;
}, 0)
return sum
}
