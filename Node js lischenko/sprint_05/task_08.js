// itgid.info - курс NODE.JS 2024


// Task 08
// Создайте анонимный модуль в данном файле, который при запуске делает читает файл ./f_05/01.json и записывает из него данные в файл ./f_05/08.json без ключа "os".

const fs = require('fs');
const path = require('path');

module.exports = function () {
       const pathFile = path.join(__dirname, "./f_05/01.json");
       const pathNewFile = path.join(__dirname, "./f_05/08.json");

       const data = fs.readFileSync(pathFile, { encoding: "utf8", flag: "r" });
       const obj = JSON.parse(data);
		 delete obj.os;

       fs.writeFileSync(pathNewFile, JSON.stringify(obj), {
         encoding: "utf8",
         flag: "w",
       });
}

