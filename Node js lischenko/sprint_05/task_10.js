// itgid.info - курс NODE.JS 2024


// Task 10
// Создайте анонимный модуль в данном файле, который получает массив, выбирает из него числа и записывает эти числа в виде массива в JSON файл:
// ./f_05/10.json.

const fs = require('fs');
const path = require('path');

module.exports = function (arr) {
const pathFile = path.join(__dirname, "./f_05/10.json");
const arrNum = arr.filter(item =>  typeof(item) === "number");
fs.writeFileSync(pathFile, JSON.stringify(arrNum), {encoding: "utf8", flag: "w"})
}

