// itgid.info - курс NODE.JS 2024


// Task 04
// Создайте анонимный модуль в данном файле, который принимает аргумент - имя файла JSON, читает указанный файл и возвращает сумму значений ключа AMOUNT.

const fs = require('fs');
const path = require('path');

module.exports = function (fileName) {
   const pathFile = path.join(__dirname, fileName);
   const data = fs.readFileSync(pathFile, { encoding: "utf8", flag: "r" });
   let obj = JSON.parse(data);
	let arr = obj.EXPENSE;
	let arrAmount = arr.map((item) => item.AMOUNT);
	return arrAmount.reduce((accun, item) => accun + item)
}

