// itgid.info - курс NODE.JS 2024


// Task 05
// Создайте анонимный модуль в данном файле, который принимает аргумент - путь к файлу JSON, и возвращает сумму его элементов.

const fs = require('fs');
const path = require('path');

module.exports = function  (fileName) {
  const pathFile = path.join(__dirname, fileName);
  const data = fs.readFileSync(pathFile, { encoding: "utf8", flag: "r" });
  let arr = JSON.parse(data);
  return arr.reduce((accum, item) => accum + item)
}

