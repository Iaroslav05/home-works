// itgid.info - курс NODE.JS 2024


// Task 03
// Создайте анонимный модуль в данном файле, который принимает аргумент - имя файла JSON, читает указанный файл и возвращает массив состоящий из имен (name) сотрудников.

const fs = require('fs');
const path = require('path');

module.exports = function (fileName) {
 const pathFile = path.join(__dirname, fileName);
 const data = fs.readFileSync(pathFile, { encoding: "utf8", flag: "r" });
 let obj = JSON.parse(data);
 let arr = obj.employees;
 return arr.map((item) => item.name);
}



