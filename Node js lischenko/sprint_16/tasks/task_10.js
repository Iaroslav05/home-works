const Sequelize = require("sequelize");
const db = require('../db');
const Processors = db.processors;

// itgid.info - курс Node.js
// Task 10
// Создайте анонимный модуль в данном файле, через ORM Sequelize реализует запрос в базу данных comp таблица processors и возвращает массив title процессоров, которые имеют 2 и 6 ядра.

module.exports = async function () {
  try {
    const processors = await Processors.findAll({
      where: {
        core: { [Sequelize.Op.between]: [2, 6] },
      },
      attributes: ["title"],
    });
    if (!processors) {
      throw new Error("No processors found");
    }
    return processors.map(processor => processor.title);
  } catch (error) {
    console.error("Error executing query:", error.message); 
    throw new Error("Database query failed");
  }
}