const Sequelize = require("sequelize");
const db = require('../db');
const Processors = db.processors;

// itgid.info - курс Node.js
// Task 07
// Создайте анонимный модуль в данном файле, через ORM Sequelize реализует запрос в базу данных comp таблица processors и возвращает массив кешей процессоров с сокетом AM4

module.exports = async function () {
  try {
    const processors = await Processors.findAll({
      where : {
        socket : 'AM4'
      }
    });
    if (!processors) {
      throw new Error("No processors found");
    } 
    return processors.map(processor => processor.cache);
  } catch (error) {
    console.error("Error executing query:", error.message);
    throw new Error("Database query failed");
  }
}