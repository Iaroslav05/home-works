const Sequelize = require("sequelize");

module.exports = function (sequelize) {
  return sequelize.define(
    "Processors",
    {
      id: {
        type: Sequelize.INTEGER.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
      },
      title: {
        type: Sequelize.STRING(300),
      },
      family: {
        type: Sequelize.STRING(300),
      },
      socket: {
        type: Sequelize.STRING(50),
        nullable: true,
      },
      core: {
        type: Sequelize.TINYINT.UNSIGNED,
        nullable: true,
      },
      video: {
        type: Sequelize.STRING(300),
        nullable: true,
      },
      cache: {
        type: Sequelize.INTEGER.UNSIGNED,
        nullable: true,
      },
      cost: {
        type: Sequelize.FLOAT.UNSIGNED,
        nullable: true,
      },
    },
    {
      timestamps: false,
      tableName: "processors",
    }
  );
};
