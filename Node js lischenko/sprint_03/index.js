const t01 = require( "./task_01" );
const t02 = require( "./task_02" );
const t03 = require( "./task_03" );
const t04 = require( "./task_04" );
const t05 = require( "./task_05" );
const t06 = require( "./task_06" );
const t07 = require( "./task_07" );
const t08 = require( "./task_08" );
const t09 = require( "./task_09" );
const t10 = require( "./task_10" );

console.log( t01() );
// console.log( t02( "sprint_02" ) );
// console.log( t03( "d_01/d_02" ) ); // пример вызова модуля из таска 3
// console.log( t04( "d_01/d_02/one.txt" ) ); // пример вызова модуля из таска 4
// console.log( t05( "d_01/d_02/one.txt" ) ); // пример вызова модуля из таска 5
// console.log( t06( "d_01/d_02/info.txt" ) );
// console.log( t07( "d_01/d_02/one.txt" ) );
console.log( t08( "d_01/d_02" ) );
// console.log( t09( "d_01/d_02" ) );
// console.log( t10( "d_01/d_02" ) );