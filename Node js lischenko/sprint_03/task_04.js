// itgid.info - курс Node.js

// Создайте анонимный модуль в данном файле, который принимает имя файла и возвращает 1, 0 в зависимости от того существует или нет указанный файл. Пример вызова указан в index.js.

const path = require( "path" );
const fs = require( "fs" );

module.exports = function ( pathToFile ) {
		return fs.existsSync( pathToFile ) ? 1 : 0;
};

