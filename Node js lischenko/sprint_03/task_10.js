// itgid.info - курс Node.js

// Создайте анонимный модуль в данном файле, который принимает имя папки и возвращает объект вида название файла - размер.

const path = require( "path" );
const fs = require( "fs" );

module.exports = function ( pathToFolder ) {
		const allFiles = fs.readdirSync( pathToFolder );
		const fileInfo = {};
		allFiles.forEach( ( item ) => {
				fileInfo[item] = fs.statSync( `${ pathToFolder }/${ item }` ).size;
				
		} );
		return fileInfo;
};

