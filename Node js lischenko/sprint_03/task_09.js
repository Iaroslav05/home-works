// itgid.info - курс Node.js

// Создайте анонимный модуль в данном файле, который принимает имя папки и возвращает массив имен файлов в данной папке.

const path = require( "path" );
const fs = require( "fs" );

module.exports = function ( pathToFolder ) {
		return fs.readdirSync( pathToFolder );
};

