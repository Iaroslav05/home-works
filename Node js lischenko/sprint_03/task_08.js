// itgid.info - курс Node.js

// Создайте анонимный модуль в данном файле, который принимает имя папки и возвращает количество файлов в ней. 

const path = require( "path" );
const fs = require( "fs" );

module.exports = function ( pathToFolder ) {
		return fs.readdirSync( pathToFolder ).length;
};

