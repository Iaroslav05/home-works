// itgid.info - курс NODE.JS 2024


// Task 10
// Напишите код, который аргументы переданные в виде
// node task_10.js --port=1500 --hostname=site.ua primer abba
// преобразует в объект вида
// {"port" : "1500", "hostname" : "site.ua"}
// значения не начинающиеся на -- отбрасывает
// порядок свойств в объекте - значения не имеет
// выведите в консоль объект

const arguments = process.argv.slice(2);
const obj = {};

arguments.forEach(argument => {
	if(argument.includes("--")){
const [key, value] = argument.replace(/--/g, "").split("=");
obj[key] = value;
	}
	
});

console.log(JSON.stringify(obj));