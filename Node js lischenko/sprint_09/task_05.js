// itgid.info - курс NODE.JS 2024


// Task 05
// Напишите код, который выводит в консоль большую строку из тех, что переданы в приложение node.js как аргументы. Количество строк - любое.
// node task_05.js abra jj bitrin
// ожидается результат jj

const arguments = process.argv.slice(2);
const maxStr = arguments.reduce((accum, item) => {
  return accum > item ? accum : item;
});
console.log(maxStr);