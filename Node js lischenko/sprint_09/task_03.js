// itgid.info - курс NODE.JS 2024

// Task 03
// Напишите код, который выводит в консоль большее из двух чисел переданных как аргументы. При равенстве - выводит первое.
// node task_03.js 10 33
// ожидается результат 33

const arguments = process.argv.slice(2);
if (arguments[0] > arguments[1]) {
  console.log(arguments[0]);
} else if (arguments[0] < arguments[1]) {
  console.log(arguments[1]);
} else {
  console.log(arguments[0]);
}
