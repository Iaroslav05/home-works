// itgid.info - курс NODE.JS 2024


// Task 07
// Напишите код, который выводит в консоль 1 если в node.js приложение переданы аргументы и 0 если не переданы.
const arguments = process.argv.slice(2);

const arg = arguments.length > 0 ? 1 : 0;


console.log(arg);