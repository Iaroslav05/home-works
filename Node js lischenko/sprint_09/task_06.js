// itgid.info - курс NODE.JS 2024


// Task 06
// Напишите код, который выводит в консоль большую строку (по длине) из тех, что переданы в приложение node.js как аргументы. Количество строк - любое.
// node task_06.js abra jj bitrin
// ожидается результат bitrin
const arguments = process.argv.slice(2);
const maxStr = arguments.reduce((accum, item) => {
  return accum.length > item.length ? accum : item;
});

console.log(maxStr);
