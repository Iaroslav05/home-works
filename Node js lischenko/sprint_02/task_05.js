// itgid.info - курс Node.js

// Создайте анонимный модуль который принимает строку и возвращает случайный символ из данной строки. Весь код реализуйте внутри функции модуля.


module.exports = function ( s ) {
		s = s.split( "" );
		let randInd = Math.floor( Math.random() * s.length );
		return s[randInd];
};