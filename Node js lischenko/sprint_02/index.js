// тут вы подключаете и тестируете модули. Подключать и отключать можно в любой последовательности.

const task1 = require( "./task_01" );
console.log( task1 );

// const task2 = require( "./task_02" );
// console.log( task2( 15, 15 ) );

// const task3 = require( "./task_03" );
// console.log( task3( [ 1, -8, 3 ] ) );

// const task4 = require( "./task_04" );
// console.log( task4( "     HeLLo woRD    " ) );

// const task5 = require( "./task_05" );
// console.log( task5( "aroslav" ) );

// const { preparedArray } = require( "./task_06" );
// console.log( preparedArray( [ "Y  ", "  a", "r", "O", "S", "l", "a", "v", 1, 2 ] ) );

const { isWhitespace } = require( "./task_07" );
console.log( isWhitespace( "" ) );

// const task8 = require( "./task_08" );
// console.log( task8( [ 3, "33", "56b" ] ) );

// const task9 = require( "./task_09" );
// console.log( task9( "jpg" ) );

// const { password } = require( "./task_10" );
// console.log( password( "  ssqwertyuio  " ) );

// const task11 = require( "./task_11" );
// console.log( task11( 0 ) );
//
// const { fileSize } = require( "./task_12" );
// console.log( fileSize( 40000000 ) );

// const task13 = require( "./task_13" );
// console.log( task13( [ 1, 2, 3, true, "a" ], [ 1, 2, 3, true, "a" ] ) );

// const { eqObject } = require( "./task_14" );
// console.log( eqObject( { car: "BMW", age: 5, gas: true }, { car: "BMW", age: 5, gas: true } ) );

// const { eqObject, eqArray } = require( "./task_15" );
// console.log( eqArray( [ 1, 2, 3, true, "a" ], [ 1, 2, 3, true, "a" ] ) );
// console.log( eqObject( { car: "BMW", age: 5, gas: true }, { car: "BMW", age: 5, gas: true } ) );