// itgid.info - курс Node.js

// Создайте анонимный модуль который принимает два целых числа и возращает большее. Если числа равны - то возвращает первое число. Модуль должен быть реализован в данном файле. 

module.exports = function ( a, b ) {
		switch ( true ) {
				case a > b:
						return a;
				case a < b:
						return b;
				default:
						return a;
		}
};