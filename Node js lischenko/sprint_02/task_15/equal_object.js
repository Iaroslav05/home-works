module.exports = function ( obj1, obj2 ) {
		const arr1 = Object.entries( obj1 ).flat();
		const arr2 = Object.entries( obj2 ).flat();
		if ( arr1.length !== arr2.length ) {
				return false;
		}
		for ( let i = 0; i < arr1.length; i++ ) {
				if ( arr1[i] !== arr2[i] ) {
						return false;
				}
		}
		return true;
};
