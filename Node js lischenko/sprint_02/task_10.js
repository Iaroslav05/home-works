// itgid.info - курс Node.js

// Создайте модуль password который принимает строку и проверяет, что ее длина больше 8 символов и символы, которые применяются в строке - уникальные в рамках строки. Возвращает true, false. Модуль должен предварительно удалить пробелы по краям строки.

module.exports.password = function ( s ) {
		s = s.trim();
		const [ ...pass ] = new Set( s );
		return pass.length > 8 && s.length === pass.length;
};