const API = "d73b49bcc5d508c76091d1572872dd9d";

showWeather();

function showWeather() {
  let id = 703448;
  const selectElement = document.getElementById("sity");
  id = selectElement.value;
  const url = `https://api.openweathermap.org/data/2.5/weather?id=${id}&units=metric&appid=${API}`;
  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      document.querySelector(".sity").textContent = data.name;
      document.querySelector(".temp").innerHTML =
        Math.round(data.main.temp) + "&deg;";
      document.querySelector(".des").textContent =
        data.weather[0]["description"];
    });
}
