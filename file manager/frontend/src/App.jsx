import { useState, useEffect } from "react";

import "./App.css";

const App = () => {
		const [ data, setData ] = useState( {
				path: "",
				files: []
		} );
		const [parent, setParent] = useState("")
		
		useEffect( () => {
				fetch( "http://localhost:8000/" )
					.then( res => res.json() )
					.then(
						( result ) => {
								setParent("")
								setData( result );
						},
						( error ) => {
								console.error( error );
						}
					);
		}, [] );
		
		const clickHandler = ( event ) => {
				event.preventDefault();
					fetch( "http://localhost:8000/?path=" + event.target.attributes.href.value )
					.then( res => res.json() )
					.then(
						( result ) => {
								let linkArr = result.path.split( "/" );
								linkArr.pop()
								setParent( linkArr.join( "/" ) );
								setData( result );
						},
						( error ) => {
								console.error( error );
						}
					);
		};
		return (
			<div className="file-mananger">
					<div>
							<a href={parent} onClick={clickHandler}>
									LEVEL UP
							</a>
							</div>
					
					<div className="current-level">
							current: {data.path === "" ? "/" : data.path}
					</div>
					
					<ul className="folder-list">
							{ data.files.map( item => {
									if ( item.dir ) {
											return <li key={ item.name } className="folder">
													<a href={ data.path + "/" + item.name } onClick={ clickHandler }>
															<span className="material-symbols-outlined">&#xe2c7;</span>
															{ item.name.toUpperCase() }
													</a>
											</li>;
									} else {
											return <li key={ item.name } className="file">
													<span className="material-icons">&#xe873;</span>
													{ item.name }
											</li>;
									}
							} ) }
					</ul>
			</div>
		);
};

export default App;


