const express = require( "express" );

const app = express();
const PORT = 8000;

// Убрати політику CORS
app.use(function (req, res, next) {
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Access-Control-Allow-Methods', 'GET');
		res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
		res.setHeader('Access-Control-Allow-Credentials', true);
		next();
});

require("./routing")(app)
app.listen(PORT, () => {
		console.log(`Port is work on localhost ${PORT}
		Heppy haking`);
})