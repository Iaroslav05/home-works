const btn = document.querySelector(".btn");

btn.addEventListener("click", () => {
  let btnRed = btn.classList.toggle("red");
  if (btnRed) {
    btn.style.background = "red";
  } else {
    btn.style.background = "inherit";
  }
});
