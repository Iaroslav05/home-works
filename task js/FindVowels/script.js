FindVowels("hello");
FindVowels("why");

const vowels = ["a", "e", "i", "o", "u"];

function FindVowels(str) {
  const vowels = ["a", "e", "i", "o", "u"];
  let count = 0;

  for (let char of str.toLowerCase()) {
    if (vowels.includes(char)) {
      count++;
    }
  }
  return count;
}

console.log(FindVowels("hello"));
console.log(FindVowels("why"));
