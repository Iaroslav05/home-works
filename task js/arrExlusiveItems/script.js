const arr = [1, 2, 2, 3, 4, 5, 6, 7, 6, 4];

const newArr = [...new Set(arr)];
console.log(newArr);

const newArr1 = arr.reduce((acc, item) => {
  return acc.includes(item) ? acc : [...acc, item];
}, []);

console.log(newArr1);

const set = new Set(arr);
let newArr2 = [];
for (item of set) {
  newArr2.push(item);
}
console.log(newArr2);
