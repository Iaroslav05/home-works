// Скільки разів зустрічається елемент в масиві
//{kivi: 3, apple: 2, orange: 1}
const fruits = ["kivi", "apple", "kivi", "orange", "apple", "kivi"];

const countFruts = (arr) => {
  const count = {};
  arr.forEach((fruit) => {
    if (!count[fruit]) {
      count[fruit] = 1;
    } else {
      count[fruit]++;
    }
  });
  return count;
};

console.log(countFruts(fruits));

//створіть масив який має тільки унікальні елементи
// ["kivi", "apple", "orange"];
const myFruits = ["kivi", "apple", "kivi", "orange", "apple", "kivi"];

const uniqueItems = (arr) => {
  let colections = new Set(arr);
  return [...colections];
};
console.log(uniqueItems(myFruits));

// створити функцію яка згрупує студентів по віку
// {
// "20": [{name: "alex", age: 20}, {name: "lola", age: 20}]
// 	"19": [{name: "max", age: 19}]
// 	"18": [{name: "viki", age: 18}]
// }

const students = [
  { name: "alex", age: 20 },
  { name: "max", age: 19 },
  { name: "lola", age: 20 },
  { name: "viki", age: 18 },
];

const groupStudents = (students) => {
  const grouped = {};
  students.forEach((student) => {
    if (!grouped[student.age]) {
      grouped[student.age] = [student];
    } else {
      grouped[student.age].push(student);
    }
  });
  return grouped;
};
console.log(groupStudents(students));

// Написати функцію яка приймає два аргумента масив чисел і суму
// Якщо сума двохлюбих чисел рівна дркгому аргументу сумма то
// вернути новий масив з цими двома числами
//  якщо нема то повернути пустий масив
//[-1, 11], [6, 4]

const myNumbers = [3, 5, 4, -1, 8, 6, 11, 10, 0];
const sum = 10;

const findPairs = (arr, target) => {
  for (let i = 0; i < arr.length; i++) {
    const firstNum = arr[i];

    for (let j = i + 1; j < arr.length; j++) {
      const secondNum = arr[j];

      if (firstNum + secondNum === target) {
        return [firstNum, secondNum];
      }
    }
  }
  return [];
};

console.log(findPairs(myNumbers, sum));

// Получити масив любимих піц друзів
// ["cheese", "papperoni", "salami", "margarita", "meat", "fish"]
const friends = [
  { name: "alex", pizzas: ["cheese", "papperoni"] },
  { name: "max", pizzas: ["salami", "margarita"] },
  { name: "lola", pizzas: ["meat"] },
  { name: "viki", pizzas: ["fish"] },
];

const pizzas = friends.reduce((accum, item) => {
  return [...accum, ...item.pizzas];
}, []);

console.log(pizzas);
