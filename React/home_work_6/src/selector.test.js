import {
	selectProjectors, selectIsModal, selectIsModalTwo,
	selectFavorites, selectCurrentGoods, selectBasket
} from './store/selectors';

const mockCards = [{
	"title": "TouYinger M4 FullHD", "price": 3700,
	"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
	"article": 12345, "color": "White", "isFavorite": false
}]

describe('test selectors', () => {
	it('test selector selectProjectors', () => {
		expect(selectProjectors({
			goods: {
				projectors: [{
					"title": "TouYinger M4 FullHD", "price": 3700,
					"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
					"article": 12345, "color": "White", "isFavorite": false
				}]
			}
		})).toEqual(mockCards)
	});

	it('test selector selectIsModal', () => {
		expect(selectIsModal({
			isModal: {
				isModal: false
			}
		})).toBe(false);
	});
	it('test selector selectIsModaTwo', () => {
		expect(selectIsModalTwo({
			isModalTwo: {
				isModalTwo: false
			}
		})).toBe(false);
	});

	it('test selector selectFavorites', () => {
		expect(selectFavorites({
			isFavorite: {
				favorites: [{
					"title": "TouYinger M4 FullHD", "price": 3700,
					"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
					"article": 12345, "color": "White", "isFavorite": false
				}]
			}
		})).toEqual(mockCards)
	});

	it('test selector selectBasket', () => {
		expect(selectBasket({
			isBasket: {
				basket: [{
					"title": "TouYinger M4 FullHD", "price": 3700,
					"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
					"article": 12345, "color": "White", "isFavorite": false
				}]
			}
		})).toEqual(mockCards)
	});

	it('test selector selectCurrentGoods', () => {
		const mockCurrentGoods = {
			"title": "TouYinger M4 FullHD", "price": 3700,
			"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
			"article": 12345, "color": "White", "isFavorite": false
		}
		expect(selectCurrentGoods({
			isBasket: {
				currentGoods: {
					"title": "TouYinger M4 FullHD", "price": 3700,
					"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
					"article": 12345, "color": "White", "isFavorite": false
				}
			}
		})).toEqual(mockCurrentGoods)
	});
});
