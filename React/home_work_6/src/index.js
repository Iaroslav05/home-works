import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from "react-router-dom";
import { Provider } from 'react-redux';

import DisplayProvider from './context/DisplayProvaider';
import { Global } from './styledIndex'
import App from './App';
import { store } from './store';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
	<React.StrictMode>
		<Global />
		<Provider store={store}>
			<BrowserRouter>
				<DisplayProvider>
					<App />
				</DisplayProvider>
			</BrowserRouter>
		</Provider>
	</React.StrictMode>
);
