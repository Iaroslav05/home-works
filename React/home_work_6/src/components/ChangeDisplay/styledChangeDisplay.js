import styled from 'styled-components';

const BasketCardsWrapper = styled.div`
display: flex;
align-items: center;
justify-content: space-around;
gap: 50px;
margin: 20px 20px 20px 50px;
box-shadow: 5px 5px 10px grey;
border-radius: 5px;
`

const CardWrapper = styled.div`
box-shadow: 5px 5px 10px grey;
border-radius: 5px;
display: inline-flex;
flex-direction: column;
align-items: center;
justify-content: space-between;
margin: 20px ;
padding: 20px;
gap: 20px;

}
`


export { CardWrapper, BasketCardsWrapper };