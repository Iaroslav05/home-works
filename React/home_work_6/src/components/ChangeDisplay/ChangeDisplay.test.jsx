import { render, screen, renderHook } from '@testing-library/react';
import { toBeInTheDocument } from '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

import DisplayProvider from '../../context/DisplayProvaider';
import { ChangeDisplay } from './ChangeDisplay';
import { Button } from '../../pages/Basket/components/Button';

describe('component Button', () => {
	it('snapshot component Button', () => {
		const component = render(
			<DisplayProvider>
				<ChangeDisplay />
				<Button />
			</DisplayProvider>
		);

		expect(component).toMatchSnapshot();
	});

	it('click button change display', () => {
		render(
			<DisplayProvider>
				<ChangeDisplay />
				<Button />
			</DisplayProvider>
		);
		expect(screen.queryByTestId('basket-tablet')).toBeNull();
		expect(screen.getByTestId('basket-cards')).toBeInTheDocument();

		userEvent.click(screen.getByTestId('icons-cart'));
		expect(screen.queryByTestId('basket-cards')).toBeNull();
		expect(screen.getByTestId('basket-tablet')).toBeInTheDocument();

		userEvent.click(screen.getByTestId('icons-tablet'));
		expect(screen.queryByTestId('basket-tablet')).toBeNull();
		expect(screen.getByTestId('basket-cards')).toBeInTheDocument();
	});
});