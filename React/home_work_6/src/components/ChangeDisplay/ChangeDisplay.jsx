import PropTypes from 'prop-types';
import { useDisplay } from '../../context/DisplayProvaider';

import { CardWrapper, BasketCardsWrapper } from './styledChangeDisplay'

const ChangeDisplay = ({ children }) => {
	const { isCart } = useDisplay()

	return (
		<>
			{!isCart ? <BasketCardsWrapper data-testid='basket-cards' >{children}</BasketCardsWrapper> : <CardWrapper data-testid='basket-tablet'>{children}</CardWrapper>}
		</>
	)
}

export { ChangeDisplay }

ChangeDisplay.propTypes = {
	children: PropTypes.object,
}
