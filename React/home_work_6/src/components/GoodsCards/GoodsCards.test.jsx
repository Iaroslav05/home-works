import { render } from '@testing-library/react';
import * as reduxHooks from 'react-redux';

import { GoodsCards } from './GoodsCards';

jest.mock('react-redux');

const mockedUseSelektor = jest.spyOn(reduxHooks, 'useSelector');

const mockCards = [{
	"title": "TouYinger M4 FullHD", "price": 3700,
	"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
	"article": 12345, "color": "White", "isFavorite": false
},
{
	"title": "Touyinger T9 FullHD", "price": 4400,
	"url": "https://images.prom.ua/3757855178_w640_h640_3757855178.jpg",
	"article": 23456, "color": "White", "isFavorite": false
}
];

describe('Cards', () => {
	it('should create Cards with empty cards', () => {
		mockedUseSelektor.mockReturnValue([]);
		const component = render(<GoodsCards />);

		expect(component).toMatchSnapshot();
	});

	it('should create Cards with cards item', () => {
		mockedUseSelektor.mockReturnValue(mockCards);
		const component = render(<GoodsCards />);

		expect(component).toMatchSnapshot();
	});
});
