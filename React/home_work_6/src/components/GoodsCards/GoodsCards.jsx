import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { CardsWrapper, CardWrapper } from './styledGoodsCards'
import { Card } from './components/Card';
import { selectProjectors, selectFavorites } from '../../store/selectors';


function GoodsCards() {

	const projectors = useSelector(selectProjectors);
	const favorites = useSelector(selectFavorites);

	return (
		<CardsWrapper>
			{projectors.map((el, index) => (
				<CardWrapper key={index}>
					<Card
						isFavorite={favorites.includes(el.article)}
						item={el}
					/>
				</CardWrapper>
			))}

		</CardsWrapper>

	);

}

GoodsCards.propTypes = {
	hendlerCurrentGoods: PropTypes.func,
}

export { GoodsCards };

