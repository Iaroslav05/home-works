import { render, screen } from '@testing-library/react';
import * as reduxHooks from 'react-redux';
import userEvent from '@testing-library/user-event';

import { Card } from './Card';
import * as actionModal from '../../../../features/modal/isModalSlice';
import * as actionsBasket from '../../../../features/basket/basketSlice';
import * as actionsFavorites from '../../../../features/favorites/favoritesSlice';

jest.mock('react-redux');

const dispatch = jest.fn();
const mockedUseSelektor = jest.spyOn(reduxHooks, 'useSelector');
const mockedUseDispatch = jest.spyOn(reduxHooks, 'useDispatch');



const mockCard = {
	"title": "TouYinger M4 FullHD", "price": 3700,
	"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
	"article": 12345, "color": "White", "isFavorite": false
};
const mockCards = [{
	"title": "TouYinger M4 FullHD", "price": 3700,
	"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
	"article": 12345, "color": "White", "isFavorite": false
}];

describe('Cards', () => {
	it('should create empty card', () => {
		mockedUseSelektor.mockReturnValue([]);
		const component = render(<Card item={[]} />
		);

		expect(component).toMatchSnapshot();
	});

	it('should create Card', () => {
		mockedUseSelektor.mockReturnValue(mockCard);
		const component = render(<Card item={mockCard} />);

		expect(component).toMatchSnapshot();
	});

	it('should action open modal window "actionModal" ', () => {
		mockedUseDispatch.mockReturnValue(dispatch);
		const mockedActionModal = jest.spyOn(actionModal, 'actionIsModal');
		const mockedAddIsCurrentGoods = jest.spyOn(actionsBasket, 'actionAddIsCurrentGoods');
		render(<Card item={mockCard} />);
		userEvent.click(screen.getByRole('button', { name: /Add to cart/i }));

		expect(dispatch).toHaveBeenCalledTimes(2);
		expect(mockedActionModal).toHaveBeenCalledWith(true);
		expect(mockedAddIsCurrentGoods).toHaveBeenCalledWith(mockCard);
	});

	it('should add/remuve favorites "actionsFavorites, actionsFavorites"', () => {
		mockedUseDispatch.mockReturnValue(dispatch);
		mockedUseSelektor.mockReturnValue(mockCards);
		const mockedActionAddIsFavorite = jest.spyOn(actionsFavorites, 'actionAddIsFavorite');
		const mockedActionRemoveIsFavorites = jest.spyOn(actionsFavorites, 'actionRemoveIsFavorites');

		render(<Card item={mockCard} />);
		userEvent.click(screen.getByTestId('btn-favorite-star'));
		expect(dispatch).toHaveBeenCalledTimes(1);
		expect(mockedActionAddIsFavorite).toHaveBeenCalledWith(12345);

		// !-- Click delete not work
		userEvent.click(screen.getByTestId('btn-favorite-star'));
		expect(dispatch).toHaveBeenCalledTimes(2);
		// expect(mockedActionRemoveIsFavorites).toHaveBeenCalledWith(mockCard);
	});

	it('should filter in favorite', () => {
		const filterTestFn = jest.fn();
		filterTestFn.mockReturnValueOnce(true).mockReturnValueOnce(false);
		const result = [11, 12].filter(num => filterTestFn(num));

		console.log(result);

		console.log(filterTestFn.mock.calls[0][0]); // 11
		console.log(filterTestFn.mock.calls[1][0]); // 12

	});
});

