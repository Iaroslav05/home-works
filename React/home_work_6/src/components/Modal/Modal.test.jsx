import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Modal from './Modal';

const mockProps = {

	closeButton: jest.fn(),
};

describe('Modal', () => {
	it('should create snapshot modal', () => {
		const component = render(<Modal />);
		expect(component).toMatchSnapshot();
	});

	it('should render title component with props', () => {
		const component = render(<Modal header='Test title' />);

		expect(component).toMatchSnapshot();
	});

	it('should render text component without props', () => {
		const component = render(<Modal text='Test text' />);


		expect(component).toMatchSnapshot();
	});

	it('should check the modal close button', () => {
		render(<Modal {...mockProps} />);
		const button = screen.getByRole('button');
		userEvent.click(button);

	});
});
