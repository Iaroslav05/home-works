import { toBeInTheDocument } from '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from "react-router-dom";
import userEvent from '@testing-library/user-event';

import App from '../App';
import { store } from '../store';
import { Card } from '../components/GoodsCards/components/Card';

const mockCard = {
	"title": "TouYinger M4 FullHD", "price": 3700,
	"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
	"article": 12345, "color": "White", "isFavorite": false
};

describe('component App', () => {
	it('snapshot component App', () => {
		const app = render(
			<Provider store={store}>
				<MemoryRouter>
					<App />
				</MemoryRouter>
			</Provider>
		);
		expect(app).toMatchSnapshot();
	});

	it('Router test', () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<App />
				</MemoryRouter>
			</Provider>
		);
		const linkHome = screen.getByTestId('home-link');
		const linkFavorites = screen.getByTestId('favorites-link');
		const linkBasket = screen.getByTestId('basket-link');

		userEvent.click(linkHome);
		expect(screen.getByTestId('home-page')).toBeInTheDocument();
		userEvent.click(linkFavorites);
		expect(screen.getByTestId('favorites-page')).toBeInTheDocument();
		userEvent.click(linkBasket);
		expect(screen.getByTestId('basket-page')).toBeInTheDocument();
	});
});
