import styled from 'styled-components';

const FavoritesCardsWrapper = styled.ul`
display: flex;
align-items: center;
justify-content: space-around;
gap: 20px;
margin: 20px;
box-shadow: 5px 5px 10px grey;
border-radius: 5px;

`

const FavoritesCardTitle = styled.p`
text-align: center;
font-size: 18px;
font-weight: 700;
`

const FavoritesGoodsColor = styled.p`
font-size: 14px;
font-weight: 700;
`

const FavoritesCardArt = styled.p`
font-size: 12px;
`

const FavoritesCardImg = styled.img`
wight: 100px;
height: 100px;
`

const FavoritesCardPrice = styled.p`
text-align: center;
font-size: 16px;
font-weight: 700;
`

export {FavoritesCardTitle, FavoritesGoodsColor, FavoritesCardArt, FavoritesCardImg, FavoritesCardPrice, FavoritesCardsWrapper }