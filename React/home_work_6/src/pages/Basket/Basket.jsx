import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { ChangeDisplay } from '../../components/ChangeDisplay/ChangeDisplay';
import { Button } from './components/Button';
import { Sidebar } from './components/Sidebar';
import { selectBasket } from '../../store/selectors';
import { BasketCard } from './components/BasketCard/';
import { BasketWrapper } from './styledBasket'


export default function Basket() {
	const basket = useSelector(selectBasket);

	return (
		<BasketWrapper data-testid='basket-page'>
			<div>
				<Button />
				{
					basket.map((el) => (
						<ChangeDisplay key={el.article} >
							<BasketCard el={el} />
						</ChangeDisplay >
					))
				}
			</div>

			<div>
				<Sidebar basket={basket} />
			</div>
		</BasketWrapper>
	)
}

Basket.propTypes = {
	basket: PropTypes.array,
	hendlerCurrentGoods: PropTypes.func
}

