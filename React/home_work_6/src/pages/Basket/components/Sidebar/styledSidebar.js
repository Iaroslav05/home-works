import styled from 'styled-components';

import { ButtonStyle } from '../../../../components/GoodsCards/components/Card';

const SidebarWrapper = styled.div`
box-shadow: 5px 5px 10px grey;
border-radius: 5px;
`

const FildsetStyled = styled.fieldset`
border: none;
`
const LegendStyled = styled.legend`
text-align: center;
font-size: 28px;
color: darkGreen;
font-weight: 700;
padding: 20px 0 0 0;
`

const ButtonForm = styled(ButtonStyle)`
display: block;
margin: 20px 110px;
width: 150px;
`

export { SidebarWrapper, FildsetStyled, LegendStyled, ButtonForm };