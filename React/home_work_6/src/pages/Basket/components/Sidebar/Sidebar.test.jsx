import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';

import Sidebar from './Sidebar';
import { store } from '../../../../store';

describe('component Sidebar', () => {
	it('snapshot component Sidebar', () => {
		const sidebar = render(
			<Provider store={store}>
				<Sidebar />
			</Provider>
		);
		expect(sidebar).toMatchSnapshot();
	});

	it('click button "Checkout"', () => {
		render(
			<Provider store={store}>
				<Sidebar />
			</Provider>
		);
		const button = screen.getByRole('button', { name: /Checkout/i });
		userEvent.click(button);
	});
});