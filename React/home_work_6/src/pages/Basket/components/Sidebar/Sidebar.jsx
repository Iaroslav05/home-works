import { Formik, Form } from "formik";
import PropTypes from 'prop-types';
import { useDispatch } from "react-redux";

import { Input, InputTextarea, InputPhone } from '../../../../components/Form';
import { validationSchema } from "./validation";
import { actionRemoveLocalstorageIsBasket } from '../../../../features/basket/basketSlice';

import { SidebarWrapper, FildsetStyled, LegendStyled, ButtonForm } from './styledSidebar';

export default function Sidebar({ basket }) {
	const dispatch = useDispatch()

	return (
		<SidebarWrapper>
			<Formik

				initialValues={
					{
						name: '',
						lastName: '',
						age: '',
						shippingAddress: '',
						phone: ''
					}
				}
				onSubmit={(values) => {
					dispatch(actionRemoveLocalstorageIsBasket(basket));
					console.log('Buyer =>', values);
					console.log('Goods =>', basket);
				}}
				validationSchema={validationSchema}
			>
				{({ errors, touched, getFieldProps }) => (
					<Form>
						<FildsetStyled>
							<LegendStyled>Placing an order</LegendStyled>
							<Input inputName={'name'} lable={'Your name'} placeholder='Enter your name' error={errors.name && touched.name} />

							<Input inputName={'lastName'} lable={'Your last name'} placeholder='Enter your last name' error={errors.lastName && touched.lastName} />

							<Input inputName={'age'} type={'number'} label={'Your age'} placeholder='Enter your age' error={errors.age && touched.age} />

							<InputTextarea lable={'Your shipping address'} nameTextArea={'shippingAddress'} rows={8} cols={23} placeholder={'Enter your shipping address'} error={errors.shippingAddress && touched.shippingAddress} />

							<InputPhone lable={'Your phone number'} inputPhoneName={'phone'} error={errors.phone && touched.phone} />

							<ButtonForm type="submit">Checkout</ButtonForm>

						</FildsetStyled>

					</Form>
				)}

			</Formik>
		</SidebarWrapper>

	)
}

Sidebar.propTypes = {
	basket: PropTypes.array
}