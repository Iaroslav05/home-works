import { render, screen } from '@testing-library/react';
import * as reduxHooks from 'react-redux';
import userEvent from '@testing-library/user-event';

import Basket from './Basket';
import * as actionModalTwo from '../../features/modal/isModalTwoSlice';
import * as actionsBasket from '../../features/basket/basketSlice';

jest.mock('react-redux');

const dispatch = jest.fn();
const mockedUseSelektor = jest.spyOn(reduxHooks, 'useSelector');
const mockedUseDispatch = jest.spyOn(reduxHooks, 'useDispatch');

const mockCard = [{
	"title": "TouYinger M4 FullHD", "price": 3700,
	"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
	"article": 12345, "color": "White", "isFavorite": false
}];

describe('Basket', () => {
	it('should create empty basket', () => {
		mockedUseSelektor.mockReturnValue([]);
		mockedUseDispatch.mockReturnValue(jest.fn());
		const component = render(<Basket />
		);

		expect(component).toMatchSnapshot();
	});

	it('should create basket', () => {
		mockedUseSelektor.mockReturnValue(mockCard);
		mockedUseDispatch.mockReturnValue(jest.fn());
		const component = render(<Basket />);

		expect(component).toMatchSnapshot();
	});
});
