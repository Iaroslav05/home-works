import isBasketSlice, {
	actionAddIsBasket,
	actionAddIsCurrentGoods,
	actionRemoveIsBasket,
	actionRemoveLocalstorageIsBasket
} from './basketSlice';

const initialState = {
	basket: (JSON.parse(localStorage.getItem('basket')) || []),
	currentGoods: {}
};

const mockCard = {
	"title": "TouYinger M4 FullHD", "price": 3700,
	"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
	"article": 12345, "color": "White", "isFavorite": false
};

describe('isBasketSlice', () => {
	it('should return default state when passed an empty actions', () => {
		const result = isBasketSlice(undefined, { type: '' });

		expect(result).toEqual(initialState);
	});

	it('should add new goods to CurrentGoods "actionAddIsCurrentGoods" action', () => {
		const action = { type: actionAddIsCurrentGoods.type, payload: mockCard };
		const result = isBasketSlice(initialState, action);

		expect(result.currentGoods).toEqual(mockCard);
	});

	it('should add new goods to basket "actionAddIsBasket" action', () => {
		const action = { type: actionAddIsBasket.type, payload: mockCard };
		const result = isBasketSlice(initialState, action);

		expect(result.basket.article).not.toEqual(mockCard.article)
		expect(result.basket).toEqual([mockCard]);
	});

	it('should remove goods to basket "actionRemoveIsBasket" action', () => {
		const action = { type: actionRemoveIsBasket.type, payload: mockCard.article };
		const result = isBasketSlice(initialState, action);
		expect(result.basket.article).not.toEqual(mockCard.article)
		expect(result.basket).toEqual([]);
	});

	it('should remove goods to localstorage is basket "actionRemoveLocalstorageIsBasket" action', () => {
		const action = { type: actionRemoveLocalstorageIsBasket.type, payload: mockCard.article };
		const result = isBasketSlice(initialState, action);
		expect(result.basket.article).not.toEqual(mockCard.article)
		expect(result.basket).toEqual([]);
	});
});
