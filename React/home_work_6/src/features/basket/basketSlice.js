import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	basket: (JSON.parse(localStorage.getItem('basket')) || []),
	currentGoods: {}
}

export const isBasketSlice = createSlice({
	name: 'isBasket',
	initialState,
	reducers: {
		actionAddIsCurrentGoods: (state, action) => {
			state.currentGoods = { ...action.payload }
		},

		actionAddIsBasket: (state, action) => {
			let isInArray = false;
			state.basket.forEach(el => {
				if (el.article === state.currentGoods.article)
					isInArray = true;
			})
			if (!isInArray)
				state.basket = [...state.basket, action.payload]

		},

		actionRemoveIsBasket: (state) => {
			state.basket = state.basket.filter(el => el.article !== state.currentGoods.article);
		},

		actionRemoveLocalstorageIsBasket: () => {
			localStorage.removeItem('basket');
			document.location.reload();
		},

	}
})

export const { actionAddIsBasket, actionAddIsCurrentGoods, actionRemoveIsBasket, actionRemoveLocalstorageIsBasket } = isBasketSlice.actions
export default isBasketSlice.reducer