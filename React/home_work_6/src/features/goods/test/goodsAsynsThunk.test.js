import { getGoods } from "../goodsSlice";

global.fetch = jest.fn();

describe('goodsThunk', () => {
	it('shoud getGoods with resolved response', async () => {
		const mockCard = [{
			"title": "TouYinger M4 FullHD", "price": 3700,
			"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
			"article": 12345, "color": "White", "isFavorite": false
		}];
		fetch.mockResolvedValue({
			ok: true,
			json: () => Promise.resolve(mockCard)
		});
		const dispatch = jest.fn();
		const thunk = getGoods();

		await thunk(dispatch, () => ({}));

		const { calls } = dispatch.mock
		expect(calls).toHaveLength(2);

		const [start, end] = calls;

		expect(start[0].type).toBe(getGoods.pending().type);
		expect(end[0].type).toBe(getGoods.fulfilled().type);
		expect(end[0].payload).toBe(mockCard);


	});

	it('shoud getGoods with rejected response', async () => {

		fetch.mockResolvedValue({
			ok: false
		});
		const dispatch = jest.fn();
		const thunk = getGoods();

		await thunk(dispatch, () => ({}));

		const { calls } = dispatch.mock
		expect(calls).toHaveLength(2);

		const [start, end] = calls;

		expect(start[0].type).toBe(getGoods.pending().type);
		expect(end[0].type).toBe(getGoods.rejected().type);
		expect(end[0].meta.rejectedWithValue).toBe(true);
		expect(end[0].payload).toBe('Server Error !');
	});
});
