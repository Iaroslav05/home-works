import isFavoriteSlice, {
	actionAddIsFavorite,
	actionRemoveIsFavorites
} from './favoritesSlice';

const initialState = {
	favorites: (JSON.parse(localStorage.getItem('favorites')) || []),
}

const mockCard = {
	"title": "TouYinger M4 FullHD", "price": 3700,
	"url": "https://images.prom.ua/3468122255_touyinger-m4-fullhd.jpg",
	"article": 12345, "color": "White", "isFavorite": false
};

describe('isFavoritesSlice', () => {
	it('should return default state when passed an empty actions', () => {
		const result = isFavoriteSlice(undefined, { type: '' });

		expect(result).toEqual(initialState);
	});

	it('should add new goods to favorites "actionAddIsFavorite" action', () => {
		const action = { type: actionAddIsFavorite.type, payload: mockCard };
		const result = isFavoriteSlice(initialState, action);

		expect(result.favorites).toEqual([mockCard]);
	});

	it('should remove goods to favorites by id "actionAddIsFavorite" action', () => {
		const action = { type: actionRemoveIsFavorites.type, payload: mockCard.article };
		const result = isFavoriteSlice(initialState, action);
		expect(result.favorites.article).not.toEqual(mockCard.article)
		expect(result.favorites).toEqual([]);
	});

});
