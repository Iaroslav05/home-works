import { renderHook } from '@testing-library/react';

import DisplayProvider, { useDisplay } from './DisplayProvaider';

describe('component DisplayProvider', () => {
	it('component DisplayProvider', () => {
		const wrapper = ({ children }) => <DisplayProvider>{children}</DisplayProvider>
		const { result } = renderHook(() => useDisplay(), { wrapper })

		expect(result.current.isCart).toBe(false)
	});
});