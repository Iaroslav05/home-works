import { useMemo } from 'react';
import { createContext, useState, useContext } from 'react';
import PropTypes from 'prop-types';


export const DisplayContext = createContext({ isCart: false });

const DisplayProvider = ({ children }) => {
	const [isCart, setIsCart] = useState(false);
	const value = useMemo(() => ({ isCart, setIsCart }), [isCart])
	return (
		<DisplayContext.Provider value={value}>
			{children}
		</DisplayContext.Provider>
	)
}

export default DisplayProvider;

export const useDisplay = () => {
	const value = useContext(DisplayContext)
	return value;
}


DisplayProvider.propTypes = {
	children: PropTypes.object,
}
