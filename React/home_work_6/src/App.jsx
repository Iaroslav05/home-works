import { useEffect } from 'react'
import { Routes, Route } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import {
	AppWrapper, ButtonModalWrapper, ButtonModal,
	MenuWrapper, LinkCustom
} from './styledApp';
import { Modal } from './components/Modal';
import { Header } from './components/Header';
import { Home } from './pages/Home';
import { Favorites } from './pages/Favorites';
import { Basket } from './pages/Basket';
import { getGoods } from './features/goods/goodsSlice';
import { actionIsModal } from './features/modal/isModalSlice';
import { actionIsModalTwo } from './features/modal/isModalTwoSlice';
import { actionAddIsBasket, actionRemoveIsBasket } from './features/basket/basketSlice';
import {
	selectProjectors, selectIsModal, selectIsModalTwo,
	selectFavorites, selectCurrentGoods, selectBasket
} from './store/selectors';


function App() {
	const dispatch = useDispatch();

	const projectors = useSelector(selectProjectors);
	const { status, error } = useSelector((state) => state.goods);
	const isModal = useSelector(selectIsModal);
	const isModalTwo = useSelector(selectIsModalTwo);
	const favorites = useSelector(selectFavorites);
	const currentGoods = useSelector(selectCurrentGoods);
	const basket = useSelector(selectBasket)

	localStorage.setItem('basket', JSON.stringify(basket));
	localStorage.setItem('favorites', JSON.stringify(favorites));

	useEffect(() => {
		dispatch(getGoods());
	}, [dispatch]);

	const hendlerModal = () => {
		dispatch(actionIsModal(!isModal))
	}

	const hendlerModalTwo = () => {
		dispatch(actionIsModalTwo(!isModalTwo))
	}

	const hendlerBasket = (currentGoods) => {
		dispatch(actionAddIsBasket(currentGoods))
		hendlerModal()
	}

	const deleteOrder = (currentGoods) => {
		dispatch(actionRemoveIsBasket(currentGoods))
		hendlerModalTwo()
	}

	const actions = (
		<ButtonModalWrapper>
			<ButtonModal type='button'
				onClick={isModal ? () => hendlerBasket(currentGoods) : () => deleteOrder(currentGoods)}>ok</ButtonModal>
			<ButtonModal type='button' backgroundColor={'#B43727'}
				onClick={isModal ? hendlerModal : hendlerModalTwo}>cancel</ButtonModal>
		</ButtonModalWrapper>
	)

	return (
		<AppWrapper>
			<Header
				countBasket={basket.length}
				countFavorite={favorites.length} />

			{status === 'loading' && <h2>Loading...</h2>}
			{error && <h2>An error occured: {error}</h2>}

			<MenuWrapper>
				<LinkCustom to="/" data-testid='home-link'>Home</LinkCustom>
				<LinkCustom to="/Favorites" data-testid='favorites-link'>Favorites</LinkCustom>
				<LinkCustom to="/Basket" data-testid='basket-link'>Basket</LinkCustom>
			</MenuWrapper>

			<Routes>
				<Route path='/' element={<Home />} />
				<Route path='/Favorites' element={<Favorites
					favorites={projectors.filter((el) => favorites.includes(el.article))}
				/>}
				/>
				<Route path='/Basket' element={<Basket
					deleteOrder={deleteOrder}
				/>}
				/>
			</Routes>

			{isModal && <Modal
				data-testid='modal'
				actions={actions}
				closeButton={hendlerModal}
				header='Ви дійсно хочете додати цей товар в корзину?'
				text="Натисніть 'ОК' для відправлення товару в корзину або натисніть 'CANCEL' для відміни операції "
				backgroundColor='darkOliveGreen'
			/>}

			{isModalTwo && <Modal
				data-testid='modal-basket'
				actions={actions}
				closeButton={hendlerModalTwo}
				header='Ви дійсно хочете видалити цей товар з корзини?'
				text="Натисніть 'ОК' для видалення товару з корзини або натисніть 'CANCEL' для відміни операції"
			/>}

		</AppWrapper >

	)
}

export default App;

