import styled from 'styled-components';
import { ReactComponent as DeleteIcons } from './icons/deleteIcons.svg';

const BasketWrapper = styled.div`
display: flex;
align-items: space-around;
justify-content: space-around
`

const BasketCardsWrapper = styled.div`
display: flex;
align-items: center;
justify-content: space-around;
gap: 50px;
margin: 20px;
box-shadow: 5px 5px 10px grey;
border-radius: 5px;

`

const BasketCardTitle = styled.p`
text-align: center;
font-size: 18px;
font-weight: 700;
`

const BasketGoodsColor = styled.p`
font-size: 14px;
font-weight: 700;
`

const BasketCardArt = styled.p`
font-size: 12px;
`

const BasketCardImg = styled.img`
wight: 100px;
height: 100px;
`

const BasketCardPrice = styled.p`
text-align: center;
font-size: 16px;
font-weight: 700;
`

const DeleteButton = styled.button`
background-color: transparent;
border: none;
cursor: pointer;
`

const Icons = styled(DeleteIcons)`
transition: transform 500ms ease;
&:hover{
	transform: scale(1.5);
} 
`

export {
	BasketWrapper,
	BasketCardTitle,
	BasketGoodsColor,
	BasketCardArt,
	BasketCardImg,
	BasketCardPrice,
	BasketCardsWrapper,
	DeleteButton,
	Icons
}