import * as yup from 'yup';

export const validationSchema = yup.object({
	name: yup
		.string('Enter your name')
		.required('Name is required')
		.min(2, "Name is too short"),
	lastName: yup
		.string('Enter your Last name')
		.required('Last name is required')
		.min(2, "Last name is too short"),
	age: yup
		.number('Enter your age')
		.required('Age is required'),

	shippingAddress: yup
		.string('Enter your address')
		.required('Shipping address is required'),

	phone: yup
		.string('Enter your phone')
		.required('Phone is required'),
})
