import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import {
	CardTitle, GoodsColor, CardArt,
	CardImg, CardPrice, CardStar, ButtonStyle
} from './styledCard';
import { ReactComponent as FavoritStar } from './icons/favoritStar.svg';
import { actionIsModal } from '../../../../features/modal/isModalSlice';
import { actionAddIsFavorite, actionRemoveIsFavorites } from '../../../../features/favorites/favoritesSlice';


function Card({ hendlerCurrentGoods, item, isFavorite }) {

	const dispatch = useDispatch();

	const isModal = useSelector((state) => state.isModal.isModal);
	const favorites = useSelector((state) => state.isFavorite.favorites);

	const hendlerModal = () => {
		dispatch(actionIsModal(!isModal))
	}

	const hendlerFavorites = (item) => {
		const isAdd = favorites.includes(item.article);
		if (isAdd) {
			dispatch(actionRemoveIsFavorites(item));
		} else {
			dispatch(actionAddIsFavorite(item.article));
		}
	}

	const starFill = isFavorite ? "#DAA520" : "#D3D3D3";


	return (
		<>
			<CardTitle>{item.title}</CardTitle>
			<GoodsColor>Колір: {item.color}</GoodsColor>
			<CardArt>Артикуль: {item.article}</CardArt>
			<CardImg src={item.url} alt={item.title} />
			<CardPrice>Ціна: {item.price} грн.</CardPrice>
			<CardStar type='button' onClick={() => {

				hendlerFavorites(item)

			}}>
				<FavoritStar style={{ fill: starFill }} />
			</CardStar>
			<ButtonStyle type='button' onClick={() => {
				hendlerModal()
				hendlerCurrentGoods(item)
			}}> Add to cart </ ButtonStyle>
		</>
	);
}

Card.propTypes = {
	item: PropTypes.object
}

export { Card, ButtonStyle };



