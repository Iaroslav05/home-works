import PropTypes from 'prop-types';
import { PatternFormat } from 'react-number-format';

import { LableStyled, LableTitleStyled, StyledField, ErrorMessegeStyled } from './styledInputPhone'

export function InputPhone({ error, lable, inputPhoneName, ...restProps }) {

	return (
		<LableStyled color={error && 'error'}>
			<LableTitleStyled>{lable}</LableTitleStyled>
			<StyledField forwardedAs={PatternFormat}
				border={error && 'error'}
				name={inputPhoneName}
				{...restProps}
				format="(###) ### ## ##" allowEmptyFormatting={true} mask="_" />
			<ErrorMessegeStyled name={inputPhoneName} component='p' />
		</LableStyled>
	)
}

InputPhone.propTypes = {
	inputPhoneName: PropTypes.string,
	label: PropTypes.string,
	error: PropTypes.object,
}
