import styled from 'styled-components';

const ModalWindow = styled.div`
display: flex;
align-items: center;
justify-content: center;
height: 100vh;
width: 100vw;
background-color: rgba(0,0,0,.3);
position: fixed;
top: 0;
left: 0;
`

const ModalBody = styled.div`
background-color: ${props => props.backgroundColor || 'tomato'};
border-radius: 5px;
height: 250px;
width: 550px;
`

const Modalheader = styled.div`
display: flex;
align-items: baseline;
justify-content: space-between;
background-color: rgba(0,0,0,.2);
padding: 20px;
`

const CloseButton = styled.button`
background-color: transparent;
border: none;
cursor: pointer;
`

const ModalTitle = styled.h2`
padding: 0 30px 0 0;
text-align: center;
font-size: 24px;
color: white;
`

const ModalText = styled.p`
text-align: center;
font-size: 16px;
padding: 20px;
color: white};
`

const ModalFooter = styled.div`
text-align: center;
font-size: 16px;
padding: 10px;
`
export { ModalWindow, ModalBody, Modalheader, CloseButton, ModalTitle, ModalText, ModalFooter }