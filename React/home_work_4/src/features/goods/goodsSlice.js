import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
	projectors: (JSON.parse(localStorage.getItem('favoritIcon')) || []),
	status: null,
	error: null,
}

export const getGoods = createAsyncThunk(
	'goods/getGoods',
	async (_, { rejectWithValue }) => {
		try {
			const res = await fetch("goods.json");
			if (!res.ok) {
				throw new Error('Server Error !')
			}
			const result = await res.json();
			return result
		} catch (error) {
			return rejectWithValue(error.message);
		}

	}
)

export const goodsSlice = createSlice({
	name: 'goods',
	initialState,
	redusers: {
		actionProjectors: (state, action) => {
			state.projectors = action.payload
		},
	},
	extraReducers: {
		[getGoods.pending]: (state) => {
			state.status = 'loading';
			state.error = null;
		},
		[getGoods.fulfilled]: (state, action) => {
			state.status = 'resolved';
			state.projectors = action.payload;
		},
		[getGoods.rejected]: (state, action) => {
			state.status = 'rejected';
			state.error = action.payload;
		}

	}
})

export const { actionProjectors } = goodsSlice.actions;
export default goodsSlice.reducer;