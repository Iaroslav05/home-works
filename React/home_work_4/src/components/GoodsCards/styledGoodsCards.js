import styled from 'styled-components';

const CardsWrapper = styled.ul`
display: grid;
grid-template-columns: repeat(4, 1fr);
gap: 20px;
padding: 20px;
`
const CardWrapper = styled.li`
position: relative;
box-shadow: 5px 5px 10px grey;
border-radius: 5px;
display: flex;
flex-direction: column;
align-items: center;
justify-content: space-between;
&:hover{
	box-shadow: 5px 5px 10px #FF7901;
}
`

export { CardsWrapper, CardWrapper };