import { GoodsCards } from '../../components/GoodsCards';

export default function Home({
	projectors,
	hendlerCurrentGoods,
	hendlerFavorites,
	toggleIcons,
	hendlerModal,
	deleteFavorites

}) {
	return (
		<GoodsCards
			projectors={projectors}
			openModal={hendlerModal}
			hendlerCurrentGoods={hendlerCurrentGoods}
			hendlerFavorites={hendlerFavorites}
			toggleIcons={toggleIcons}
			deleteFavorites={deleteFavorites}

		/>

	)
}
