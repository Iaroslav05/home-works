import PropTypes from 'prop-types';

import {
	CardTitle, GoodsColor, CardArt,
	CardImg, CardPrice, CardStar, ButtonStyle
} from './styledCard';
import { ReactComponent as FavoritStar } from './icons/favoritStar.svg';


function Card({ openModal,
	hendlerCurrentGoods,
	hendlerFavorites,
	item,
	toggleIcons,
	pos,
	isFavorite,
	deleteFavorites }) {
	const starFill = isFavorite ? "#DAA520" : "#D3D3D3";
	return (
		<>
			<CardTitle>{item.title}</CardTitle>
			<GoodsColor>Колір: {item.color}</GoodsColor>
			<CardArt>Артикуль: {item.article}</CardArt>
			<CardImg src={item.url} alt={item.title} />
			<CardPrice>Ціна: {item.price} грн.</CardPrice>
			<CardStar type='button' onClick={() => {
				!isFavorite ? hendlerFavorites(item) : deleteFavorites(item)

				toggleIcons(pos)

			}}>
				<FavoritStar style={{ fill: starFill }} />
			</CardStar>
			<ButtonStyle type='button' onClick={() => {
				openModal()
				hendlerCurrentGoods(item)
			}}> Add to cart </ ButtonStyle>
		</>
	);

}

Card.propTypes = {
	openModal: PropTypes.func,
	hendlerCurrentGoods: PropTypes.func,
	hendlerFavorites: PropTypes.func,
	item: PropTypes.object
}

export { Card, ButtonStyle };



