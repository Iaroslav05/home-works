import { Component } from 'react';
import { Button, ButtonStyle } from './components/Button';
import { Modal } from './components/Modal';
import styled from 'styled-components';

const AppWrapper = styled.div``;

const ButtonModalWrapper = styled.div`
display: flex;
justify-content: space-around;
`;

const ButtonModal = styled(ButtonStyle)`
width: 150px;
`;

const ButtonWrapper = styled.div`
display: flex;
justify-content: center;
margin: 25px 0 0 0;
gap: 35px;
text-transform: uppercase;
`;

class App extends Component {

	state = {
		isModalOne: false,
		isModalTwo: false
	}

	hendlerModalOne = () => {
		this.setState((prevState) => {
			return {
				...prevState,
				isModalOne: !prevState.isModalOne
			}
		})
	}

	hendlerModalTwo = () => {
		this.setState((prevState) => {
			return {
				...prevState,
				isModalTwo: !prevState.isModalTwo
			}
		})
	}

	render() {

		const { isModalOne, isModalTwo } = this.state;

const actions = (
	<ButtonModalWrapper>
		<ButtonModal type='button' backgroundColor = {isModalTwo && '#b43727'}>ok</ButtonModal>
		<ButtonModal type='button' backgroundColor = {isModalTwo && '#b43727'} 
		onClick={ isModalOne ? this.hendlerModalOne : this.hendlerModalTwo}>cancel</ButtonModal>
	</ButtonModalWrapper>
)

		return (
			<AppWrapper>
				<ButtonWrapper>
					<Button text='Open first modal' onClick={this.hendlerModalOne} backgroundColor='darkOliveGreen' />
					<Button text='Open second modal' onClick={this.hendlerModalTwo} backgroundColor='#b43727' />
				</ButtonWrapper>

				{isModalOne && <Modal
				actions={actions}
					closeButton={this.hendlerModalOne}
					header='Do you want to add this file?'
					text="After adding this file, it will be displayed on your page. 
					Are you sure you want to add it?"
					color='white'
					backgroundColor='darkOliveGreen' />}

				{isModalTwo && <Modal
				actions={actions}
					closeButton={this.hendlerModalTwo}
					header='Do you want to delete this file?'
					text="Once you delete this file, it won't be possible to undo this action.	
					Are you sure you want to delete it?"
					color='white' />}

			</AppWrapper>
		);
	}
}

export default App;
