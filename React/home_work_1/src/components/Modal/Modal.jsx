import { Component, createRef } from 'react';
import styled from 'styled-components'

const ModalWindow = styled.div`
display: flex;
align-items: center;
justify-content: center;
height: 100vh;
width: 100vw;
background-color: rgba(0,0,0,.3);
position: fixed;
top: 0;
left: 0;
`;

const ModalBody = styled.div`
border-radius: 5px;
background-color: ${props => props.backgroundColor || 'tomato'};
height: 250px;
width: 520px;
`;

const Modalheader = styled.div`
display: flex;
align-items: center;
justify-content: space-between;
background-color: rgba(0,0,0,.2);
padding: 20px;
`;

const CloseButton = styled.button`
background-color: transparent;
border: none;
cursor: pointer;
`;

const ModalTitle = styled.h2`
font-size: 24px;
color:  ${props => props.color || 'black'};
`;

const ModalText = styled.p`
text-align: center;
font-size: 16px;
padding: 20px;
color:  ${props => props.color || 'black'};
`;

const ModalFooter = styled.div`
text-align: center;
font-size: 16px;
padding: 20px;
color:  ${props => props.color || 'black'};
`;

class Modal extends Component {

	constructor() {
		super();

		this.wrapperRef = createRef();
		this.handleClickOutside = this.handleClickOutside.bind(this);
	}

	componentDidMount() {
		document.addEventListener("mousedown", this.handleClickOutside);
	}

	componentWillUnmount() {
		document.removeEventListener("mousedown", this.handleClickOutside);
	}



	handleClickOutside(event) {
		if (this.wrapperRef && !this.wrapperRef.current.contains(event.target))
			this.props.closeButton()
	}


	render() {
		const { closeButton, actions, text, header } = this.props;
		return (
			<ModalWindow >
				<ModalBody ref={this.wrapperRef} {...this.props}>
					<Modalheader>
						<ModalTitle {...this.props}>
							{header}
						</ModalTitle>
						<CloseButton onClick={closeButton}>
							<svg viewBox="0 0 16 16" width="24" height="24" fill='white'>
								<path d="m9.3 8 6.1-6.1c.4-.4.4-.9 0-1.3s-.9-.4-1.3 0L8 6.7 1.9.6C1.6.3 1 .3.6.6c-.3.4-.3 1 0 1.3L6.7 8 .6 14.1c-.4.4-.3.9 0 1.3l.1.1c.4.3.9.2 1.2-.1L8 9.3l6.1 6.1c.4.4.9.4 1.3 0s.4-.9 0-1.3L9.3 8z" />
							</svg>
						</CloseButton>
					</Modalheader>
					<ModalText {...this.props}>
						{text}
					</ModalText>
					<ModalFooter />
					{actions}
				</ModalBody>
			</ModalWindow>
		)
	}
}

export default Modal;
