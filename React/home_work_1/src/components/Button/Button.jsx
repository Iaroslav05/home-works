import { Component } from 'react';
import styled from 'styled-components';

const ButtonStyle = styled.button`
background-color: ${props => props.backgroundColor || 'darkGreen'};
color: white;
text-transform: uppercase;
padding: 15px 25px;
border: none;
border-radius: 5px;
cursor: pointer;
`;

class Button extends Component {
	render() {
		const { text } = this.props

		return < ButtonStyle type='button' {...this.props}> {text} </ ButtonStyle>

	}
}

// export default Button;
export  {Button, ButtonStyle};