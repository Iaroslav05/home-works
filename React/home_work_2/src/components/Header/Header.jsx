import { Component } from 'react';
import PropTypes from 'prop-types';

import { HeaderStyle, HeaderTitle, SpanIcons } from './styledHeader'
import { ReactComponent as HeaderStar } from './icons/headerStar.svg';
import { ReactComponent as Basket } from './icons/basket.svg';

class Header extends Component {
	render() {
		const { countFavorite, countBasket } = this.props;

		return (
			<HeaderStyle>
				<HeaderTitle>Інтернет-магазин 'Зебра'</HeaderTitle>
				<HeaderStar />
				<SpanIcons> {countFavorite}</SpanIcons>
				<Basket />
				<SpanIcons> {countBasket}</SpanIcons>
			</HeaderStyle>
		)
	}
}

Header.propTypes = {
	countFavorite: PropTypes.number,
	countBasket: PropTypes.number

}

Header.defaultProps = {
	countFavorite: 0,
	countBasket: 0
};

export default Header;
