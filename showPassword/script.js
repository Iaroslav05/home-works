const showPassBtn = document.querySelector( "#show-pass-btn" );
const passInput = document.querySelector( "#password" );

const TIME_SHOW_PASSWORD = 5000;

function disableBtm() {
		// ставим атрибут disabled якщо поле інпута пусте і знімаєм якщо не пусте
		showPassBtn.disabled = passInput.value === "";
}

disableBtm();

if ( showPassBtn && passInput ) showPassBtn.addEventListener( "click", showPassword );

function showPassword() {
		if ( passInput.classList.contains( "animate-password" ) ) return;
		
		passInput.setAttribute( "type", "text" );
		passInput.classList.add( "animate-password" );
		
		let interval = setInterval( () => {
				passInput.setAttribute( "type", "password" );
				passInput.classList.remove( "animate-password" );
				clearInterval( interval );
		}, TIME_SHOW_PASSWORD );
}