'use strict';


const person = new Employee('Ярослав', 37, 900);

const programmer = new Programmer('Ярослав', 37, 900, ['html', 'css', 'scss', 'js']); 
const programmer1 = new Programmer('Павло', 25, 500, ['html', 'css']); 
const programmer2 = new Programmer('Віктор', 30, 700, ['html', 'css', 'js']); 
const programmer3 = new Programmer('Влад', 33, 800, ['html', 'css', 'scss']); 

// console.log(person);

console.log(programmer);
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);


