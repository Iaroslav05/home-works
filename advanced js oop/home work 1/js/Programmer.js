class Programmer extends Employee {
	constructor(name, age, salary, lang){
		super(name, age);
			this.lang = lang;
			this.salary = salary;
		}

get name(){
		return this._name;
		}

	set name(name){
		return this._name = name;
	}

	get age(){
		return this._age;
	}

	set age(age){
		return this._age = age;
	}

	get salary(){
		return  this._salary;
	}

	set salary(salary){
		return this._salary = `${salary}` * 3;
		 
	}
}