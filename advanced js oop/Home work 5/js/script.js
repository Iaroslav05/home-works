"use strict";

import Card from './modules/Card.js';

const container = document.createElement('div');
container.classList.add('container');
document.body.prepend(container);

const buttonAddPost = document.createElement('button');
buttonAddPost.innerText = 'Додати публікацію';
container.before(buttonAddPost);

	buttonAddPost.addEventListener('click', (body) => {
const titlePost = prompt('Enter the title of your post', 'title');
		const bodyPost = prompt('Enter your post', 'body');

		fetch(`https://ajax.test-danit.com/api/json/posts`, {
    method: 'POST',
    body: JSON.stringify({
	id: 1,
      userId: 1,
			title: titlePost,
			body: bodyPost,
			name: "Leanne Graham",
			email: "Sincere@april.biz",
    }),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(response => response.json())
		.then(json => console.log(json))
				container.insertAdjacentHTML('afterbegin',
			`<div class="card">
		<h4 class="title-post"> ${titlePost}</h4>
		<p class="body-post">${bodyPost}</p>
		<p class="user">"Leanne Graham"</p>
		<p class="user-email">email: Sincere@april.biz"</p>
		</div>`)
			});



fetch(`https://ajax.test-danit.com/api/json/users/`)
	.then((response => response.json()))
	.then((data) => {
		data.forEach(({ id, name, email }) => {
			fetch(`https://ajax.test-danit.com/api/json/posts`)
				.then((response => response.json()))
				.then((posts) => {
					const postsArr = posts.filter(({ userId }) => {
					if (userId === id) {
							return true;
						} else {
							return false;
						}
					})
					postsArr.forEach(({ id, title, body }) => {
						new Card(id, name, email, title, body).render(container);
					})
			})
		})
	})

