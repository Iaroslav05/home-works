
class Card {
	constructor(id, name, email, title, body) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.title = title;
		this.body = body;
		this.buttonDelete = document.createElement('button');
	}


	render(parent) {
	
			parent.insertAdjacentHTML('beforeend',
			`<div class="card">
		<h4 class="title-post"> ${this.title}</h4>
		<p class="body-post">${this.body}</p>
		<p class="user">${this.name}</p>
		<p class="user-email">email: ${this.email}</p>

			</div>`)
		
		document.querySelectorAll('.card').forEach((el) => {
			el.append(this.buttonDelete);
			this.buttonDelete.innerText = 'Delete';
		})

		this.buttonDelete.addEventListener('click', () => {
		fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
								method: 'DELETE',
							})
				.then(response => console.log(response))
			document.querySelector('.card').remove();
		})
		
	}
		

}
	
export default Card;

