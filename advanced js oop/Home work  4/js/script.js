"use strict";

const container = document.createElement('div');
container.classList.add('container');
const title = document.querySelector('.title');
title.after(container);

function sendRequest(url) {
	return fetch(url)
		.then((response) => {
			return response.json();
		})
		.catch((e) => {
			console.error(`Something is bad ---> ${e}`);
		});
}
sendRequest('https://ajax.test-danit.com/api/swapi/films')
	.then((data) => {
		data.map(({ episodeId, name, openingCrawl, characters }) => {
			const film = document.createElement('div');
			film.className = 'film';
			film.insertAdjacentHTML(
				"beforeend",
				`<p>Episode: ${episodeId} </p>
						<h3>${name}</h3>
						<p class="loader"></p>
						<ul>
						</ul>
						<p>${openingCrawl}</p>`);
			container.append(film);
			const character = Promise.all(characters.map((url) => sendRequest(url)))
				.then((character) => {
					film.children[2].style.display = 'none';
					character.forEach(({ name }) => {
						film.children[3].insertAdjacentHTML(
							"beforeend",
							`<li class="loader" style="display: none";></li>
							<li class="heroes"> ${name} </li>`
						)
					});
				});
		});
	})
	.catch((e) => {
		console.error(`Something is bad ---> ${e}`);
	});



// const xhr = new XMLHttpRequest();
// xhr.open('get', 'https://ajax.test-danit.com/api/swapi/films');
// xhr.addEventListener('load', () => {
// 	const response = JSON.parse(xhr.responseText)
// 	console.log(response);
// 	const container = document.createElement('div');
// 	container.classList.add('container');
// 	document.body.prepend(container);
// 	response.forEach((post) => {
// 		const films = document.createElement('div');
// 		films.classList.add('films');
// 		const filmTitle = document.createElement('h4');
// 		filmTitle.classList.add('filmTitle');
// 		filmTitle.textContent = `Назва фільму: ${post.name}`;
// 		const episode = document.createElement('p');
// 		episode.classList.add('episode');
// 		episode.textContent = `Епізод: ${post.episodeId}`;
// 		const filmText = document.createElement('p');
// 		filmText.classList.add('filmText');
// 		filmText.textContent = `Короткий зміст: ${post.openingCrawl}`;
// 			container.prepend(films);
// 		films.append(filmTitle);
// 		films.append(episode);
// 		films.append(filmText);
// 	});
// });
// xhr.addEventListener('error', () => {
// 	console.log('error');
// });
// xhr.send();