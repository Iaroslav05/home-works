'use strict';
import Books from './modules/Books.js';
import NoAuthor from './modules/NoAuthor.js';
import NoName from './modules/NoName.js';
import NoPrice from './modules/NoPrice.js';




const books = [
	{
		author: "Люсі Фолі",
		name: "Список запрошених",
		price: 70
	},
	{
		author: "Сюзанна Кларк",
		name: "Джонатан Стрейндж і м-р Норрелл",
	},
	{
		name: "Дизайн. Книга для недизайнерів.",
		price: 70
	},
	{
		author: "Алан Мур",
		name: "Неономікон",
		price: 70
	},
	{
		author: "Террі Пратчетт",
		name: "Рухомі картинки",
		price: 40
	},
	{
		author: "Анґус Гайленд",
		name: "Коти в мистецтві",
	}
];

const container = document.querySelector('#root');

let bookList = document.createElement('ul');

books.forEach(({ author, name, price }) => {
	try {
		if (author === undefined) {
			throw new NoAuthor(author);
		}

		if (name === undefined) {
			throw new NoName(name);
		}

		if (price === undefined) {
			throw new NoPrice(price);
		}
		new Books(author, name, price).render(bookList);
	} catch (err) {
		if (err.nameErr === 'Data error') {
			console.warn(err.message);
		}
	}
})
container.append(bookList);



