class NoName extends Error {
	constructor(name) {
		super();
		this.nameErr = 'Data error';
		this.message = `The name is not specified: ${name}`;
	}
}

export default NoName;