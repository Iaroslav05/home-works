'use strict';

class Books {
	constructor(author, name, price) {
		this.author = author;
		this.name = name;
		this.price = price;
	}

	render(parent) {
		parent.insertAdjacentHTML('afterbegin', `<li> ${this.author}, ${this.name}, ${this.price} </li>`)
	}
}


export default Books;