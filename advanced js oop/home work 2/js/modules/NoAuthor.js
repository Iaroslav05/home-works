class NoAuthor extends Error {
	constructor(author) {
		super();
		this.nameErr = 'Data error';
		this.message = `The author is not specified: ${author}`;
	}
}

export default NoAuthor;