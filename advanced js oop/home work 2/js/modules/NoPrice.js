class NoPrice extends Error {
	constructor(price) {
		super();
		this.nameErr = 'Data error';
		this.message = `The price is not specified: ${price}`;
	}
}

export default NoPrice;