"use strict";

const btn = document.querySelector('.btn').onclick = getIp;
const userData = document.querySelector('.user-data');

async function getIp(){
	try{
		const response = await fetch('https://api.ipify.org/?format=json');

		const data = await response.json();


	const query = data.ip;

const res =  await fetch(`http://ip-api.com/json/${query}?fields=1572889`);
const infoUserIp = await res.json();

userData.insertAdjacentHTML("beforeend", `<ul>
<li>Continent: ${infoUserIp.continent}</li>
<li>Country: ${infoUserIp.country} </li>
<li>Region name: ${infoUserIp.regionName} </li>
<li>City: ${infoUserIp.city} </li>
<li>District: ${infoUserIp.district} </li>
</ul>`);
} catch(err){
console.log('Error >>>', err);
		}
	}


