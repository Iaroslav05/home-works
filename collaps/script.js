// Ми не знаємо скільки кнопок може бути для колапса то будем працювати черех делегування
document.querySelector(".container").addEventListener("click", handlerCollapse);

function handlerCollapse(event) {
		if(!event.target.classList.contains("collapse-button")) return;
		const attr = event.target.getAttribute("data-toggle")
		const collapseBody = document.querySelector("#" + attr)
		if(!collapseBody) return;
		collapseBody.classList.toggle("hide")
}