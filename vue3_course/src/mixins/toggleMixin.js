export default {
  props: {
    modelValue:{
      type: Boolean,
      default: false
    },
  },

  methods: {
    hideDialog() {
      this.$emit("update:modelValue", false);
    }
  },

  mounted() {}
}
