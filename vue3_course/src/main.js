import { createApp } from 'vue'
import router from "@/router/router";
import directives from '@/directives';
import { createPinia } from "pinia";
import storeVueX from './storeVueX';

import App from '@/App.vue'
import components from '@/components/UI';


const pinia = createPinia();
const app = createApp(App);

components.forEach(component => {
  app.component(component.name, component);
});

directives.forEach(directive => {
  app.directive(directive.name, directive);
});

app
.use(router)
.use(pinia)
.use(storeVueX)
.mount('#app')
