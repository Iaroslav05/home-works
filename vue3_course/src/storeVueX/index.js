import { createStore } from "vuex";
import { postsModule } from "@/storeVueX/postsModule";

export default createStore({
  modules: {
    posts: postsModule
  }
})