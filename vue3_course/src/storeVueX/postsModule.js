import axios from "axios";

export const postsModule = {
  state: () => ({
    posts: [],
    isPostLoading: false,
    page: 1,
    limit: 10,
    totalPage: 0,
    searchQuery: "",
    selectedSort: "",
    sortOptions: [
      { value: "id", name: "By id" },
      { value: "title", name: "By name" },
      { value: "body", name: "By description" },
    ],
  }),

  getters: {
    sortedPosts(state) {
      return [...state.posts].sort((post1, post2) => {
        if (typeof post1[state.selectedSort] === "number") {
          return post1[state.selectedSort] - post2[state.selectedSort]; // Числове порівняння для id
        } else {
          return post1[state.selectedSort]?.localeCompare(
            post2[state.selectedSort]
          ); // Для рядків
        }
      });
    },

    sortedAndSearchedPosts(state, getters) {
      return getters.sortedPosts.filter((post) =>
        post.title.toLowerCase().includes(state.searchQuery.toLowerCase())
      );
    },
  },

  mutations: {
    setPosts(state, posts) {
      state.posts = posts;
    },
    setLoading(state, bool) {
      state.isPostLoading = bool;
    },
    setPage(state, page) {
      state.page = page;
    },
    setSelectedSort(state, selectedSort) {
      state.selectedSort = selectedSort;
    },
    setTotalPage(state, totalPage) {
      state.totalPage = totalPage;
    },
    setSearchQuery(state, searchQuery) {
      state.searchQuery = searchQuery;
    },
    addPost(state, post) {
      state.posts.push(post);
    },
    deletePost(state, post) {
      state.posts = state.posts.filter((p) => p.id !== post.id);
    },
  },
    actions: {
      async fetchPosts({ state, commit }) {
        try {
          commit("setLoading", true);
          const response = await axios.get(
            "https://jsonplaceholder.typicode.com/posts",
            {
              params: {
                _page: state.page,
                _limit: state.limit,
              },
            }
          );
          commit("setTotalPage", Math.ceil(
            response.headers["x-total-count"] / state.limit
          ));
          commit("setPosts", response.data);
        } catch (e) {
          console.log(e);
        } finally {
          commit("setLoading", false);
        }
      },
      //load more posts
      async loadMorePosts({state, commit}) {
        try {
          commit("setPage", state.page + 1);
          const response = await axios.get(
            "https://jsonplaceholder.typicode.com/posts",
            {
              params: {
                _page: state.page,
                _limit: state.limit,
              },
            }
          );
          commit("setTotalPage", Math.ceil(
            response.headers["x-total-count"] / state.limit
          ));
          commit("setPosts", [...state.posts, ...response.data]);
        } catch (e) {
          console.log(e);
        }
      },
    },
    namespaced: true,
  };

