import { defineStore } from "pinia";
import { ref, computed } from "vue";

export const usePostsStore = defineStore("posts", () => {
  const posts = ref([]);
  const isPostLoading = ref(true);
  const page = ref(1);
  const limit = ref(10);
  const totalPage = ref(0);
  const searchQuery = ref("");
  const selectedSort = ref("");
  const sortOptions = ref([
      { value: "id", name: "By id" },
      { value: "title", name: "By name" },
      { value: "body", name: "By description" },
    ]);
  
  const sortedPosts = computed(() => {
      return [...posts.value].sort((post1, post2) => {
        if (typeof post1[selectedSort.value] === "number") {
          return post1[selectedSort.value] - post2[selectedSort.value]; // Числове порівняння для id
        } else {
          return post1[selectedSort.value]?.localeCompare(
            post2[selectedSort.value]
          ); // Для рядків
        }
      });
    });

  const sortedAndSearchedPosts = computed(() => {
      return sortedPosts.value.filter((post) =>
        post.title.toLowerCase().includes(searchQuery.value.toLowerCase())
      );
    },
  );

  const setPosts = (newPosts) => {
    posts.value = newPosts;
  };

  const setLoading = (bool) => {
    isPostLoading.value = bool;
  };

  const setPage = (newPage) => {
    page.value = newPage;
  };

  const setSelectedSort = (newSelectedSort) => {
    selectedSort.value = newSelectedSort;
  };

  const setTotalPage = (newTotalPage) => {
    totalPage.value = newTotalPage;
  };

  const setSearchQuery = (newSearchQuery) => {
    searchQuery.value = newSearchQuery;
  };

  const addPost = (post) => {
    posts.value.push(post);
  };

  const deletePost = (post) => {
    posts.value = posts.value.filter((p) => p.id !== post.id);
  };


 const fetchPosts = async () => {
   try {
     setLoading(true);
     const response = await axios.get(
       "https://jsonplaceholder.typicode.com/posts",
       {
         params: {
           _page: page.value,
           _limit: limit.value,
         },
       }
     );
     setTotalPage(Math.ceil(response.headers["x-total-count"] / limit.value));
     setPosts(response.data);
   } catch (e) {
     console.log(e);
   } finally {
     setLoading(false);
   }
 };

 const loadMorePosts = async () => {
   try {
     setPage(page.value + 1);
     const response = await axios.get(
       "https://jsonplaceholder.typicode.com/posts",
       {
         params: {
           _page: page.value,
           _limit: limit.value,
         },
       }
     );
     setTotalPage(Math.ceil(response.headers["x-total-count"] / limit.value));
     setPosts([...posts.value, ...response.data]);
   } catch (e) {
     console.log(e);
   }
 };


  return { 
    posts,
    isPostLoading,
    page,
    limit,
    totalPage,
    searchQuery,
    selectedSort,
    sortOptions,
    sortedPosts,
    sortedAndSearchedPosts,
    setPosts,
    setLoading,
    setPage,
    setSelectedSort,
    setTotalPage,
    setSearchQuery,
    addPost,
    deletePost,
    fetchPosts,};
});
