import { ref, computed } from "vue";

export default function useSortedPosts(posts) {
 const selectedSort = ref('');
 
 const sortedPosts = computed(() => {
   return [...posts.value].sort((post1, post2) => {
     if (typeof post1[selectedSort.value] === "number") {
       return post1[selectedSort.value] - post2[selectedSort.value]; // Числове порівняння для id
     } else {
       return post1[selectedSort.value]?.localeCompare(
         post2[selectedSort.value]
       ); // Для рядків
     }
   });
 });
return { selectedSort, sortedPosts };
}