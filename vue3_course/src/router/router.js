import { createRouter, createWebHistory } from 'vue-router'

import Main from '@/pages/Main'
import PostsPage from '@/pages/PostsPage';
import PostPage from '@/pages/PostPage';
import About from '@/pages/About';
import PostsPageWithVueX from '@/pages/PostsPageWithVueX.vue';
import PostsPageWithPinia from '@/pages/PostsPageWithPinia.vue';
import PostsPageCompositionApi from '@/pages/PostsPageCompositionApi.vue';

const routes = [
  {
    path: "/",
    component: Main,
  },
  {
    path: "/posts",
    component: PostsPage,
  },
  {
    path: "/about",
    component: About,
  },
  {
    path: "/posts/:id",
    component: PostPage,
  },
  {
    path: "/vuex",
    component: PostsPageWithVueX,
  },
  {
    path: "/pinia",
    component: PostsPageWithPinia,
  },
  {
    path: "/composition",
    component: PostsPageCompositionApi,
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router