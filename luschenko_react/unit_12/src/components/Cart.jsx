import "./cart.css";

const Cart = ({ articul, title, cost, quantity, image, calculateTotalPrice }) => {
  return (
    <tr className="table-row">
      <td className="table-cell">{title}</td>
      <td className="table-cell">{cost}</td>
      <td className="table-cell">{quantity}</td>
      <td className="table-cell">
        <img className="table-image" src={image} alt={title} />
      </td>
      <td className="table-cell">
        {calculateTotalPrice(cost, quantity).toFixed(2)}
      </td>
      <td className="table-cell">
        <button className="minus-to-cart" data-key={articul}>
          -
        </button>
        <button className="remove-from-cart" data-key={articul}>
          Remove
        </button>
      </td>
    </tr>
  );
};

export default Cart;
