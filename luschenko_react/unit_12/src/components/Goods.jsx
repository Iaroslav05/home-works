const Goods = ({ articul, title, cost, image }) => {
  return (
    <div className="goods-block">
      <img src={image} alt="" />
      <p>{title}</p>
      <p>{cost}</p>
      <button className="add-to-cart" data-key={articul}>
        Add to cart
      </button>
    </div>
  );
};

export default Goods;
