import { createSlice } from "@reduxjs/toolkit";

import goodsArr from "../data/goods.json";

const initialState = {
  goods: goodsArr,
};

export const goodsSlice = createSlice({
  name: "goods",
  initialState,
  reducers: {},
});

export const {} = goodsSlice.actions;
export const selectGoods = (state) => state.goods.goods;
export default goodsSlice.reducer;
