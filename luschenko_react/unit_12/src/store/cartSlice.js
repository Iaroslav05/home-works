import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: {},
};

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    increment: (state, action) => {
      let articul = action.payload;
      if (state.value[articul] === undefined) state.value[articul] = 0;
      state.value[articul]++;
    },

    minus: (state, action) => {
      let articul = action.payload;
      if (state.value[articul] === undefined) state.value[articul] = 0;
      if (state.value[articul] > 0) {
        state.value[articul]--;
      }
      if (state.value[articul] === 0) delete state.value[articul];
    },
    deleteGood: (state, action) => {
      let articul = action.payload;
      delete state.value[articul];
    },
  },
});

export const { increment, minus, deleteGood } = cartSlice.actions;
export const selectCart = (state) => state.cart.value;
export default cartSlice.reducer;
