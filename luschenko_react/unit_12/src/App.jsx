import GoodsList from "./containers/GoodsList";
import CartList from "./containers/CartList";

import "./App.css";


function App() {
  return (
    <>
      <GoodsList />
      <CartList />
    </>
  );
}

export default App;
