import { useDispatch, useSelector } from "react-redux";

import { selectGoods } from "../store/goodsSlice";
import { increment } from "../store/cartSlice";
import Goods from "../components/Goods";

const GoodsList = () => {
  const dispatch = useDispatch();
  const goods = useSelector(selectGoods);

  const clickHandler = (event) => {
    event.preventDefault();
    const target = event.target;
    const key = target.dataset.key;
    if(!target.classList.contains('add-to-cart')) return;
    dispatch(increment(key));
  };

  return (
    <>
      <div className="goods-field" onClick={clickHandler}>
        {goods.map((item) => (
          <Goods key={item.articul} {...item} />
        ))}
      </div>
    </>
  );
};

export default GoodsList;
