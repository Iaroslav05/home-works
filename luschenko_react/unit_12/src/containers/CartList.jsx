import { useSelector, useDispatch } from "react-redux";
import { selectGoods } from "../store/goodsSlice";
import { selectCart, minus, deleteGood } from "../store/cartSlice";

import Cart from "../components/Cart";

import "./cardList.css";

const CartList = () => {
  const dispatch = useDispatch();
  const goods = useSelector(selectGoods);
  const cart = useSelector(selectCart);

  const isCartEmpty = Object.keys(cart).length === 0;

 const clickHandler = (event) => {
   event.preventDefault();
   const target = event.target;
   const key = target.dataset.key;

   if (target.classList.contains("minus-to-cart")) {
     dispatch(minus(key));
   } else if (target.classList.contains("remove-from-cart")) {
     dispatch(deleteGood(key));
   }
 };

  const goodsObj = goods.reduce((acc, item) => {
    acc[item["articul"]] = item;
    return acc;
  }, {});

  const calculateTotalPrice = (price, quantity) => {
    return price * quantity;
  };

  return (
    <>
      {isCartEmpty ? (
        <h3 className="cart-title">Cart is empty</h3>
      ) : (
        <>
          <h3 className="cart-title">Cart</h3>
          <div className="table-container" onClick={clickHandler}>
            <table className="shopping-cart-table">
              <thead>
                <tr>
                  <th className="table-header">Product Name</th>
                  <th className="table-header">Price per Unit</th>
                  <th className="table-header">Quantity</th>
                  <th className="table-header">Image</th>
                  <th className="table-header">Total Price</th>
                  <th className="table-header"></th>
                </tr>
              </thead>
              <tbody>
                {Object.keys(cart).map((item) => (
                  <Cart
                    key={item + goodsObj[item]["title"]}
                    articul={goodsObj[item]["articul"]}
                    title={goodsObj[item]["title"]}
                    cost={goodsObj[item]["cost"]}
                    quantity={cart[item]}
                    image={goodsObj[item]["image"]}
                    calculateTotalPrice={calculateTotalPrice}
                  />
                ))}
              </tbody>
            </table>
          </div>
        </>
      )}
    </>
  );
};

export default CartList;
