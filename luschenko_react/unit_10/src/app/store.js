import { configureStore } from '@reduxjs/toolkit'

import taskReducerSlice from './taskReducerSlice'

export const store = configureStore({
  reducer: {
    taskReducer: taskReducerSlice
  },
})