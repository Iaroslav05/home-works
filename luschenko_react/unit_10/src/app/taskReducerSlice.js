import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: 0,
  text: "Hello",
};

export const taskReducerSlice = createSlice({
  name: "store",
  initialState,
  reducers: {
    add: (state, action) => {
      state.value += +action.payload;
    },
    minus: (state, action) => {
      state.value -= +action.payload;
    },
    multiply: (state, action) => {
      state.value *= +action.payload;
    },
    divide: (state, action) => {  
      state.value /= +action.payload;
    },
    showConsole: (state, action) => {
     state.text = action.payload;
    },
  },
});

export const { add, minus, multiply, divide, showConsole } = taskReducerSlice.actions;

export const selectValue = (state) => state.taskReducer.value;
export const selectTest = (state) => state.taskReducer.text;

export default taskReducerSlice.reducer;
