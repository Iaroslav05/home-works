import { useSelector } from "react-redux";

import {selectValue, selectTest} from "./app/taskReducerSlice";

const Out = () => {
  const task1 = useSelector(selectValue);
  const text = useSelector(selectTest);
  return (
    <div>
      <hr />
      <h2>Task 1</h2>
      <p>{task1}</p>
      <hr />
      <p>{text}</p>
    </div>
  );
}

export default Out
