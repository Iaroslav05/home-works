import { useRef } from 'react'
import { useDispatch } from 'react-redux'

import { add, minus, multiply, divide, showConsole } from "./app/taskReducerSlice";

const In = () => {
const task1Input = useRef(null);
const task1Input1 = useRef(null);
const dispatch = useDispatch();

  const handleAdd = () => {
    dispatch(add(task1Input.current.value));
  }

  const handleMinus = () => {
    dispatch(minus(task1Input.current.value));
  }

  const handleMultiply = () => {
   dispatch(multiply(task1Input.current.value));  
  }

  const handleDivide = () => {
    dispatch(divide(task1Input.current.value));
  }

  const textHandler = () => {
    dispatch(showConsole(task1Input1.current.value));
  }

  return (
    <>
      <div>
        <input type="number" ref={task1Input}/>
        <h2>Task 1</h2>
        <button onClick={handleAdd}>+</button>
        <button onClick={handleMinus}>-</button>
        <button onClick={handleMultiply}>*</button>
        <button onClick={handleDivide}>/</button>
      </div>
      <div>
        <input type="text" ref={task1Input1}/>
        <button onClick={textHandler}>Text</button>
      </div>
    </>
  );
}

export default In
