import { useState } from "react";

import "./App.css";

function App() {
  const [T1, setT1] = useState("");
  const [T2, setT2] = useState("");
  const [T3, setT3] = useState("");
  const [T4, setT4] = useState("");
  const [T5, setT5] = useState([]);

  const URL = "http://sprint14.ua/api.php";
  async function task1() {
    try {
      const response = await fetch(URL, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: JSON.stringify({
          action: 1,
        }),
      });
      const result = await response.text();
      setT1(result);
    } catch (error) {
      console.error("Error fetch:", error);
    }
  }

  async function task2(event) {
    event.preventDefault();
    try {
      const num1 = event.target.num1.value;
      const num2 = event.target.num2.value;
      const response = await fetch(URL, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: JSON.stringify({
          action: 2,
          num1,
          num2,
        }),
      });
      const result = await response.text();
      setT2(result);
    } catch (error) {
      console.error("Error fetch:", error);
    }
  }

  async function task3(event) {
    event.preventDefault();
    try {
      const filename = event.target.filename.value;
      const filedata = event.target.filedata.value;

      const response = await fetch(URL, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: JSON.stringify({
          action: 3,
          filename,
          filedata,
        }),
      });
      const result = await response.text();
      const recordedBytes = parseInt(result, 10);

      if (recordedBytes > 0) {
        setT3(filename);
      } else {
        setT3(false);
      }
    } catch (error) {
      console.error("Error fetch:", error);
    }
  }

  async function task4(event) {
    event.preventDefault();
    try {
      const response = await fetch(URL, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: JSON.stringify({
          action: 4,
        }),
      });
      const result = await response.text();
      setT4(result);
    } catch (error) {
      console.error("Error fetch:", error);
    }
  }

  async function task5(event) {
    event.preventDefault();
    try {
      const response = await fetch(URL, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: JSON.stringify({
          action: 5,
        }),
      });
      const result = await response.json();
      console.log(result);
      setT5(result);
    } catch (error) {
      console.error("Error fetch:", error);
    }
  }

  return (
    <div>
      <h1>ItGid.info</h1>
      {/* <p>{text}</p> */}
      <hr />
      <div>
        <h2>Время сервера</h2>
        <button onClick={task1}>GET</button>
        <p>{T1}</p>
      </div>
      <hr />
      <div>
        <h2>Случайное число между</h2>
        <form action="" onSubmit={task2}>
          <div>
            <input type="number" name="num1" defaultValue="30" />
          </div>
          <div>
            <input type="number" name="num2" defaultValue="44" />
          </div>
          <button type="sumbit">Push</button>
        </form>
        <p>{T2}</p>
      </div>
      <hr />
      <div>
        <h2>Создание файла</h2>
        <form action="" onSubmit={task3}>
          <div>
            <input type="text" name="filename" />
          </div>
          <div>
            <input type="text" name="filedata" />
          </div>
          <button type="sumbit">Push</button>
        </form>
        <p>
          {T3 === false ? (
            ""
          ) : (
            <a href={`${URL}/files/${T3}`} download>
              {T3}
            </a>
          )}
        </p>
      </div>
      <hr />
      <div>
        <h2>Получение данных компьютера</h2>
        <form action="" onSubmit={task4}>
          <button type="sumbit">Push</button>
        </form>
        <p>{T4}</p>
      </div>
      <hr />
      <div>
        <h2>Получение курса валют</h2>
        <form action="" onSubmit={task5}>
          <button type="sumbit">Push</button>
        </form>
        <ul>
          {T5.map((item, index) => (
            <li key={index}>
              {item.ccy} - {item.buy} / {item.sale}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default App;
