import { Component } from 'react'

export class Homework1 extends Component {
  constructor({ p1 }) {
    super()
    console.log(p1);
    this.state = {
      s1: p1
    }
    this.s2 = 201
  }

  componentDidMount() {
    let val = this.state.s1
    val += 5
    this.setState({
      s1: val
    })
    this.s2 += 5
  }

  buttonHandler = () => {
    this.setState({
      s1: this.state.s1 + 50
    })
    this.s2 += 50
  }
  render() {
    return (
      <>
        <div>{this.state.s1}</div>
        <div>{this.s2}</div>
        <button onClick={this.buttonHandler}>Push</button>
      </>
    )
  }
}

export default Homework1
