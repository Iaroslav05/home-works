import { useState, useEffect } from "react";

const CommentHook = () => {
  const [data, setData] = useState([]);

  const fetchData = async (id = 1) => {
    const res = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${id}/comments`
    );
    const data = await res.json();
    setData(data);
  };

  useEffect(() => {
    fetchData();
  }, []);
  
  const handleSelectChange = (event) => {
    const id = event.target.value;
    fetchData(id);
  };

  return (
    <>
      <select onChange={handleSelectChange}>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
      </select>
      {data.map((el) => (
        <section key={el.id}>
          <h2>
            {el.id} {el.name}
          </h2>
          <p>{el.body}</p>
        </section>
      ))}
    </>
  );
};

export default CommentHook;
