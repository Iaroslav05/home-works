import { useState, useEffect } from 'react'

const Comments2ListHook = ({ dataArr }) => {
  const [data, setData] = useState(dataArr);

  useEffect(() => {
    if(!dataArr) return
    setData(dataArr)
  }, [dataArr]);
  const onlyEven = () => {
    const evenPost = dataArr.filter((el) => el.id % 2 === 0);
    setData(evenPost);
  }
  return (
    <div>
      <div>
        <button onClick={onlyEven}>Only even comments</button>
      </div>

      {data.map((el) => (
        <section key={el.id}>
          <h2>
            {el.id} {el.name}
          </h2>
          <p>{el.body}</p>
        </section>
      ))}
    </div>
  );
}

export default Comments2ListHook
