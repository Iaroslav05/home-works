import { useState, useEffect } from "react";

const PlaceholderPostHook = () => {
  const [data, setData] = useState([]);
  const fetchData = async () => {
    const res = await fetch(
      "https://jsonplaceholder.typicode.com/users/1/posts"
    );
    const data = await res.json();
    setData(data);
  };

  useEffect(() => {
    fetchData();
  }, []);

return (
  <>
    {data.map((el) => (
      <section key={el.id}>
        <h2>
          {el.id} {el.title}
        </h2>
        <p>{el.body}</p>
      </section>
    ))}
  </>
);
};

export default PlaceholderPostHook;
