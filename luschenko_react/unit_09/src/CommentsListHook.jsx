import React from "react";

const CommentsListHook = ({ data }) => {
  return (
    <div>
      {data.map((el) => (
        <section key={el.id}>
          <h2>
            {el.id} {el.name}
          </h2>
          <p>{el.body}</p>
        </section>
      ))}
    </div>
  );
};

export default CommentsListHook;
