import { useState, useEffect } from "react";
// import CommentsListHook from "./CommentsListHook";
import Comments2ListHook from "./Comments2ListHook";

const Comment2Hook = () => {
  const [data, setData] = useState([]);

  const fetchData = async (id = 1) => {
    const res = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${id}/comments`
    );
    const data = await res.json();
    setData(data);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSelectChange = (event) => {
    const id = event.target.value;
    fetchData(id);
  };

  return (
    <>
      <p>Choose post ID:</p>
      <select onChange={handleSelectChange}>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
      </select>
      <div>
        {/* <CommentsListHook data={data} /> */}
        <Comments2ListHook dataArr={data} />

      </div>
    </>
  );
};

export default Comment2Hook;
