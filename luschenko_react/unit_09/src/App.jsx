import PlaceholderPostHook from "./PlaceholderPostHook";
import CommentHook from "./CommentHook";
import Comment2Hook from "./Comment2Hook";

import "./App.css";

const App = () => {
  return (
    <>
      <PlaceholderPostHook />
      <CommentHook />
      <Comment2Hook />

    </>
  )
}

export default App


