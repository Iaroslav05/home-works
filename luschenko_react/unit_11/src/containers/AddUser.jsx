import { useDispatch } from "react-redux";

import { addNewUsers } from "../action";

const AddUser = () => {
  const dispatch = useDispatch();

  const formHandler = (event) => {
    event.preventDefault();
    const { passport, name, age } = event.target;
    dispatch(addNewUsers(passport.value, name.value, age.value));
  };

  return (
    <div>
      <form onSubmit={formHandler}>
        <div>
          <input
            defaultValue='JH7654321'
            name="passport"
            type="text"
            placeholder="Enter your passport number"
          />
        </div>
        <div>
          <input
            defaultValue='Pavel'
            name="name"
            type="text"
            placeholder="Enter your name"
          />
        </div>
        <div>
          <input
            defaultValue='30'
            name="age"
            type="text"
            placeholder="Enter your age"
          />
        </div>
        <button type="submit">Add</button>
      </form>
    </div>
  );
};

export default AddUser;
