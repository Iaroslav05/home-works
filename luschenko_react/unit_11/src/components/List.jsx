import React from 'react'

const List = (data) => {
  return (
    <div>
      {data.user.map((el, index) => <ul key={index}>
        <li>{el.passport}</li>
        <li>{el.name}</li>
        <li>{el.age}</li>
      </ul>)}
    </div>
  )
}

export default List
