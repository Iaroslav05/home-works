import AddUser from '../containers/AddUser';
import './App.css';
import UserList from '../containers/UserList';

function App() {
  return (
    <div>
     <AddUser />
     <UserList />
    </div>
  );
}

export default App;
