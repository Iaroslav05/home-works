import { useRef, useState } from "react";

import "./App.css";

function App() {
  const [isActive, setIsActive] = useState(false);
  const [checkboxValue, setCheckboxValue] = useState('');
  const [selectValueOut, setSelectValueOut] = useState('');
  const [rangeValue, setrangeValue] = useState(null);
  const [inputValueOut, setInputValueOut] = useState('');
  const squareColor = useRef(null);
  const selectValue = useRef(null);
  const inputValue = useRef(null);
  let count4 = 0;

  function task1() {
    console.log("task2");
  }
  function task2() {
    setIsActive(true);
  }
  function task3(event) {
    console.log(event.target.value);
  }
  function task4() {
    console.log(count4++);
  }
  function task5(event) {
    if (event.target.checked) {
      setCheckboxValue(event.target.value);
    } else {
      setCheckboxValue(0);
    }
  }
  function task6() {
    setSelectValueOut(selectValue.current.value);
  }
  function task7() {
    const rgb = `${randomInt(0, 255)},${randomInt(0, 255)},${randomInt(0, 255)}`;
    console.log(squareColor);
    squareColor.current.style.backgroundColor = `rgb(${rgb})`;
  }
  const task8 = (event) => {
    const key = event.key;
    if (!isNaN(key)) {
      setInputValueOut((prev) => prev + "1");
    } else {
      setInputValueOut((prev) => prev + "0");
    }
  };
  function task9(event) {
    setrangeValue(event.target.value);
  }
  let ar10 = [5, 6, 7];
  function task10() {
    const inputValueOut = +inputValue.current.value;
    ar10.push(inputValueOut);
    console.log(ar10);
  }

  function randomInt(a,b) {
    return Math.floor(Math.random() * (b - a + 1)) + a;
  }

  return (
    <>
      <h1>События</h1>
      <section>
        <h2>Task 1</h2>
        <button onClick={task1} className="task-1">
          Push
        </button>
      </section>
      <section>
        <h2>Task 2</h2>
        <div
          onMouseEnter={task2}
          className={`task-2 ${isActive ? "active" : ""}`}
        ></div>
      </section>
      <section>
        <h2>Task 3</h2>
        <input onInput={task3} type="text" className="task-3" />
      </section>
      <section>
        <h2>Task 4</h2>
        <button onClick={task4} className="task-4">
          Count
        </button>
      </section>
      <section>
        <h2>Task 5</h2>
        <input onChange={task5} type="checkbox" value="55" />
        <div className="out-5">{checkboxValue}</div>
      </section>
      <section>
        <h2>Task 6</h2>
        <select onChange={task6} className="task-6" ref={selectValue}>
          <option value="7">seven</option>
          <option value="4">four</option>
          <option value="9">nine</option>
          <option value="10">ten</option>
        </select>
        <div className="out-6">{selectValueOut}</div>
      </section>
      <section>
        <h2>Task 7</h2>
        <div className="block-7" ref={squareColor}></div>
        <button onClick={task7} className="task-7">Color</button>
      </section>
      <section>
        <h2>Task 8</h2>
        <input onKeyUp={task8} type="text" className="task-8" ref={inputValue}></input>
        <div className="out-8">{inputValueOut}</div>
      </section>
      <section>
        <h2>Task 9</h2>
        <input onChange={task9} type="range" className="task-9"></input>
        <div className="out-9">{rangeValue}</div>
      </section>
      <section>
        <h2>Task 10</h2>
        <input type="number" className="i-10" ref={inputValue}></input>
        <button onClick={task10} className="task-10">Push</button>
      </section>
    </>
  );
}

export default App;
