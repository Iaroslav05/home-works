import { useState, useRef } from "react";

import "./App.css";

function App() {
  const [st1, setSt1] = useState("");
  const [st2, setSt2] = useState(0);
  const [st3, setSt3] = useState("");
  const [st4, setSt4] = useState(0);
  const [st5, setSt5] = useState(null);
  const [st6, setSt6] = useState("");
  const [st7, setSt7] = useState(null);
  const [st8, setSt8] = useState("");
  const [st9, setSt9] = useState("");
  const [st10, setSt10] = useState([]);

  const ref1 = useRef(null);
  const ref2 = useRef(null);
  const ref3 = useRef(null);

  function task1() {
    setSt1(ref1.current.value);
  }
  function task2() {
    setSt2((prev) => prev + 1);
  }
  function task3(event) {
    setSt3(event.target.value);
  }
  function task4() {
    setSt4((prev) => prev + 1);
  }
  function task5(event) {
    if (event.target.checked) {
      setSt5(event.target.value);
    } else {
      setSt5(0);
    }
  }
  function task6(event) {
    setSt6(event.target.value);
  }
  function task7() {
    const rgb = `(${randomInt(0, 255)},${randomInt(0, 255)},${randomInt(
      0,
      255
    )})`;
    setSt7(`rgb${rgb}`);
  }
  function task8(event) {
    const key = event.key;
    if (!isNaN(key)) {
      setSt8((prev) => prev + "1");
    } else {
      setSt8((prev) => prev + "0");
    }
  }
  function task9(event) {
    setSt9(event.target.value);
  }
  function task10() {
    const inputValueOut = ref3.current.value;
    setSt10([...st10, inputValueOut]);
  }
  function randomInt(a, b) {
    return Math.floor(Math.random() * (b - a + 1)) + a;
  }

  return (
    <>
      <h1>События</h1>
      <section>
        <h2>Task 1</h2>
        <input type="text" ref={ref1} />
        <button onClick={task1} className="task-1">
          Push
        </button>
        <div>{st1}</div>
      </section>
      <section>
        <h2>Task 2</h2>
        <div onMouseEnter={task2} className="task-2"></div>
        <div>{st2}</div>
      </section>
      <section>
        <h2>Task 3</h2>
        <input onInput={task3} type="text" className="task-3" />
        <div>{st3}</div>
      </section>
      <section>
        <h2>Task 4</h2>
        <button onClick={task4} className="task-4">
          Count
        </button>
        <div>{st4}</div>
      </section>
      <section>
        <h2>Task 5</h2>
        <input onChange={task5} type="checkbox" value="55" />
        <div>{st5}</div>
      </section>
      <section>
        <h2>Task 6</h2>
        <select onChange={task6} className="task-6">
          <option value="7">seven</option>
          <option value="4">four</option>
          <option value="9">nine</option>
          <option value="10">ten</option>
        </select>
        <div>{st6}</div>
      </section>
      <section>
        <h2>Task 7</h2>
        <div
          className="block-7"
          style={{ backgroundColor: st7 }}
          ref={ref2}
        ></div>
        <button onClick={task7} className="task-7">
          Color
        </button>
        <div>{st7}</div>
      </section>
      <section>
        <h2>Task 8</h2>
        <input onKeyUp={task8} type="text" className="task-8"></input>
        <div>{st8}</div>
      </section>
      <section>
        <h2>Task 9</h2>
        <input onInput={task9} type="range" className="task-9"></input>
        <div>{st9}</div>
      </section>
      <section>
        <h2>Task 10</h2>
        <input type="number" className="i-10" ref={ref3}></input>
        <button onClick={task10} className="task-10">
          Push
        </button>
        <div>
          <ul>
            {st10.map((item, index) => (
              <li key={index.toString()}>{item}</li>
            ))}
          </ul>
        </div>
      </section>
    </>
  );
}

export default App;
