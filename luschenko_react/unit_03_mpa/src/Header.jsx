import React from 'react'

const Header = ({mainNav}) => {
  return (
    <nav>
      <ul>
        {mainNav.map(({ name, path }) => (
          <li key={path}>
            <a href={path}>{name}</a>
          </li>
        ))}
      </ul>
    </nav>
  );
}

export default Header
