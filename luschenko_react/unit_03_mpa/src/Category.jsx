import React from 'react'
import { useLocation } from 'react-router-dom';

const Category = ({ categoryNav }) => {
  const { pathname } = useLocation();
  return (
    <div>
     <ul>
        {categoryNav.map(({ name, path }) => (
          <li key={path}>
            <a href={`${pathname}${path}`}>{name}</a>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Category
