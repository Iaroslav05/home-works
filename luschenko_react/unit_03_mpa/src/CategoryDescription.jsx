import React from 'react'
import { useParams } from 'react-router-dom'

const CategoryDescription = () => {
  const { categoryId } = useParams();
  return (
    <div>
      <a href="/cat">Back</a>
      <h1>Category: {categoryId}</h1>
    </div>
  );
}

export default CategoryDescription
