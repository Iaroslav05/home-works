import React from "react";

const Cart = ({ cart, goods }) => {
  const renderObj = () => {
    let out = [];
    for (let key in cart) {
      let goods = getGoodsFromArr(key);
      console.log(goods);
      out.push(
        <tr key={key}>
          <td>{goods["title"]}</td>
          <td>{cart[key]}</td>
          <td>{cart[key] * goods["cost"]}</td>
        </tr>
      );
    }
    return out;
  };

  const getGoodsFromArr = (art) => {
     for (let i = 0; i < goods.length; i++) {
       if (art === goods[i]["articul"]) {
         return goods[i];
       }
     }
  };

  return (
    <div className="cart-field">
      <h1>Корзина</h1>
      <table>
        <tbody>
          <tr>
            <th>Art</th>
            <th>Count</th>
            <th>Cost</th>
          </tr>
          {renderObj()}
        </tbody>
      </table>
    </div>
  );
};

export default Cart;
