import { useState } from "react";

import Goods from "./Goods/Goods";
import Cart from "./Cart/Cart";

import goodsArr from "./bd/goods.json";
import "./App.css";

const App = () => {
  const [cart, setCart] = useState({});
  const [count, setCount] = useState(0);

  let showCart;
  if (count !== 0) {
    showCart = <Cart cart={cart} goods={goodsArr} />;
  } else {
    showCart = "Empty";
  }

  const addToCart = (event) => {
    event.preventDefault();
    if (!event.target.classList.contains("add-to-cart")) return;
    let cartTemp = cart;
    cartTemp[event.target.dataset.key]
      ? cartTemp[event.target.dataset.key]++
      : (cartTemp[event.target.dataset.key] = 1);
    setCart(cartTemp);

    let countTemp = count;
    countTemp++;
    setCount(countTemp);
    console.log(cart, count);
  };

  return (
    <div className="container" onClick={addToCart}>
      <h1>Cart</h1>
      <div className="goods-field">
        {goodsArr.map((item) => (
          <Goods
            key={item.articul}
            image={item.image}
            title={item.title}
            cost={item.cost}
            articul={item.articul}
          />
        ))}
      </div>
      {showCart}
    </div>
  );
};

export default App;
