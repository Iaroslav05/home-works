import React from "react";
import Nav from "../Header/Nav";

function Footer({ site }) {
  return (
    <footer>
      <h3>{site.site_title}</h3>
      <Nav nav={site.nav} />
    </footer>
  );
}

export default Footer;
