import { createElement, useState, useRef } from "react";
import "./App.css";
const App = () => {
  const [inputValue, setInputValue] = useState(55);
  const [liItems, setLiItems] = useState([]);
  const inputRef = useRef(null);

    const cE = createElement;

  const h1 = cE("h1", {}, " unit_08");
  const h2 = cE(
    "h2",
    {
      className: "text-orange",
    },
    "header 2"
  );
  const p = cE(
    "p",
    {
      style: {
        color: "red",
      },
    },
    "this is p"
  );

  const input = cE("input", {
    value: inputValue,
    onChange: (event) => {
      setInputValue(event.target.value);
    },
  });
  const p1 = cE("p", {}, "hi");
  const p2 = cE("p", {}, "world");
  const div = cE(
    "div",
    {
      className: "text-grey",
    },
    p1,
    p2
  );

  const input1 = cE("input", {
    ref: inputRef,
  });

   const button = cE("button", {
    onClick: () => {
        const inputValueRef = inputRef.current.value;
        if (inputValueRef) {
          setLiItems((prevItems) => [...prevItems, inputValueRef]);
          inputRef.current.value = ""; 
        }
    },
  }, "Push");

  return (
    <>
      {h1}
      {h2}
      {p}
      {input}
      {div}
      {input1}
      {button}
      <ul>
        {liItems.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
      </ul>
    </>
  );
};

export default App;
