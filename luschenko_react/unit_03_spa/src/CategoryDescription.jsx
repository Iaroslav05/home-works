import React from 'react'
import { useParams, Link } from 'react-router-dom'

const CategoryDescription = () => {
  const { categoryId } = useParams();
  return (
    <div>
      <Link to="/cat">Back</Link>
      <h1>Category: {categoryId}</h1>
    </div>
  );
}

export default CategoryDescription
