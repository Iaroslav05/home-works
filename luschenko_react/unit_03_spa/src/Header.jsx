import React from 'react'
import { NavLink } from 'react-router-dom'
const Header = ({mainNav}) => {
  return (
    <nav>
      <ul>
        {mainNav.map(({ name, path }) => (
          <li key={path}>
            <NavLink to={path}>{name}</NavLink>
          </li>
        ))}
      </ul>
    </nav>
  );
}

export default Header
