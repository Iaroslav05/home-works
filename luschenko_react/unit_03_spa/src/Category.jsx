import React from 'react'
import { useLocation, Link } from 'react-router-dom';

const Category = ({ categoryNav }) => {
  const { pathname } = useLocation();
  return (
    <div>
     <ul>
        {categoryNav.map(({ name, path }) => (
          <li key={path}>
            <Link to={`${pathname}${path}`}>{name}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Category
