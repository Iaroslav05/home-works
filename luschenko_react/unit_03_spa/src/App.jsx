import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Header from "./Header";
import Footer from "./Footer";
import Home from "./Home";
import About from "./About";
import Category from "./Category";
import ErrorPage from "./ErrorPage";
import CategoryDescription from "./CategoryDescription";

import "./App.css";

const mainNav = [
  { name: "Home", path: "/" },
  { name: "About", path: "/about" },
  { name: "Category", path: "/cat" },
];

const categoryNav = [
  { name: "Laptops", path: "/notebook" },
  { name: "Monitors", path: "/monitor" },
  { name: "Mobile phones", path: "/cellphone" },
];

const App = () => {
  return (
    <div className="app">
      <Router>
        <Header mainNav={mainNav} />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/cat" element={<Category categoryNav={categoryNav} />} />
          <Route path="/cat/:categoryId" element={<CategoryDescription />} />
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </Router>
      <Footer />
    </div>
  );
};

export default App;
