// const inputs = document.querySelectorAll( "input[type=\"number\"]" );
//
// inputs.forEach( item => item.addEventListener( "wheel", inputWheel ) );


// Це якщо просто збільши зменшив колесиком
// function inputWheel( event ) {
// 		event.preventDefault();
// 		event.deltaY < 0 ? this.value = (+this.value) + 1 : this.value = (+this.value) - 1;
// }

// Якщо потрібний шаг наприклад step = 10
// function inputWheel( event ) {
// 		event.preventDefault();
// 		let step = this.getAttribute("step") ? +this.getAttribute("step") : 1
// 		event.deltaY < 0 ? this.value = (+this.value) + step : this.value = (+this.value) - step;
// }

document.body.addEventListener( "wheel", inputWheel );

function inputWheel( event ) {
		if(event.target.classList.contains("inputNum")) {
				document.body.style.position = 'fixed';
				let step = this.getAttribute("step") ? +this.getAttribute("step") : 1
				event.deltaY < 0 ? this.value = (+this.value) + step : this.value = (+this.value) - step;
		} else {
				document.body.style.removeProperty("position");
		}
}